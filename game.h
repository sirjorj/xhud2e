#pragma once
#include "imagegen.h"
#include "settings.h"
#include <libxwing2/squad.h>
#include <array>

using namespace libxwing2;

class Game {
 public:
  Game(std::array<Squad, 2>& p, SettingsManager &s);
  void Setup();
  void Run();

 private:
  ImageGenToken token;
  std::array<Squad, 2>& players;
  //std::string outFile;
  bool isRunning;
  bool ParseCommand(std::string cmd);
};
