#pragma once
#include "settings.h"
#include <libxwing2/squad.h>
#include <string>

using namespace libxwing2;

struct ImageGenToken {
  SettingsManager &settings;
  Squad& p1squad;
  Squad& p2squad;

  // add more stuff here...

  uint8_t initiative;

  //Dice dice[2];

  //Card card;
};



void GenerateImage(Squad& list, std::string name);
void GenerateImage(ImageGenToken token);

std::string GetImageGenInfo(std::string imgGenFmt);
