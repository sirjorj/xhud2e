#pragma once
#include <string>
#include <vector>



std::vector<std::string> tokenize(std::string s, const char DELIMITER=' ');

std::string trim(std::string s);
