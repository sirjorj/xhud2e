#pragma once
#include <map>
#include <optional>
#include <regex>
#include <string>
#include <vector>



// *** SettingType ***
enum class STp {
  Bool,
  Path,
  Text,
};

class SettingTypeNotFound : public std::runtime_error {
 public:
  SettingTypeNotFound(STp k);
};

class SettingType {
public:
  static SettingType GetSettingType(STp s);
  STp         GetType();
  std::string GetName();
  std::regex  GetRegex();
  std::string GetDesc();

private:
  STp         type;
  std::string name;
  std::regex  regex;
  std::string desc;

  static std::vector<SettingType> settingTypes;

  SettingType(STp         t,
	      std::string n,
	      std::regex  r,
	      std::string d);
};



// *** SettingsManager ***
class SettingNotFound : public std::runtime_error {
 public:
  SettingNotFound(std::string k);
};

//class InvalidSetting : public std::runtime_error {
// public:
//  InvalidSetting(std::string k, std::string v);
//};



struct SettingsRecord {
  std::string key;
  STp         type;
  std::string defaultVal;
  std::string desc;
};



class SettingsManager {
 public:
  SettingsManager(std::string file);
  std::vector<std::string> GetAllKeys() const;
  void VerifySettings(bool verbose) const;
  std::string GetSetting(std::string key) const;
  std::optional<std::string> GetSettingFromConfig(std::string key) const;
  void SetSetting(std::string key, std::string value);
  void WriteSettings();
  void Dump();

 private:
  std::string filename;
  std::vector<SettingsRecord>        settingsRecords; // these are all the things in you can set and all the info about them
  std::map<std::string, std::string> settingsData; // these are the actual settings values stored in memory
  std::optional<SettingsRecord> GetSettingRecord(std::string) const;
};
