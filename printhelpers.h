#pragma once
#include <libxwing2/release.h>
#include <libxwing2/squad.h>
#include <libxwing2/release.h>
#include <libxwing2/upgrade.h>
#include <algorithm>

using namespace libxwing2;

std::string GetDifficultyColor(Difficulty d);
std::string GetArc(Arc a);

void        PrintManeuverChart(Maneuvers maneuvers);
void        PrintShips(std::vector<Ship> ships=Ship::GetAllShips());
void        PrintShip(Ship ship);
void        PrintPilots(std::vector<Pilot> pilots=Pilot::GetAllPilots());
void        PrintPilot(Pilot p);
void        PrintUpgrades(std::vector<Upgrade> upgrades=Upgrade::GetAllUpgrades());
void        PrintUpgrade(Upgrade u);
void        PrintRelease();
void        PrintRelease(std::string sku);
void        PrintSquad(Squad s);
