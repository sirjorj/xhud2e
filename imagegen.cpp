#include "imagegen.h"
#include "imagegen_base.h"
#include "imagegen_gd.h"
#include <libxwing2/color.h>
#include <libxwing2/glyph.h>
//#include <gd.h>
#include <string.h>



std::shared_ptr<ImageGenerator::ImageGen> GetImageGen(std::string format, int w, int h) {
  if(0) {}
  else if(format == "gd") {  return std::make_shared<ImageGenerator::ImageGen_GD>(w,h); }
  else { throw std::runtime_error("Unknown output format: '" + format + "'"); }
}

static int SQUAD_WIDTH  =  381;
static int SQUAD_HEIGHT = 1080;

static int IMG_WIDTH  = 1920;
static int IMG_HEIGHT = 1080;


// remove all this color stuff eventually
#if 0
struct ColorPalette {
  // background
  int clear;
  int bg;
  int bgNone;
  int bgNoneD;
  int bgRebel;
  int bgImperial;
  int bgScum;
  // destroyed
  int destroyed;
  // basics
  int white;
  int black;
  // dice
  int attackDice;
  int defenseDice;
  // diff
  int stress;
  int stressD;
  int neutral;
  int neutralD;
  // stats
  int pilot;
  int pilotD;
  int cost;
  int costD;
  int initiative;
  int initiativeD;
  int attack;
  int attackD;
  int agility;
  int agilityD;
  int hull;
  int hullD;
  int shield;
  int shieldD;
  int charge;
  int chargeD;
  int force;
  int forceD;
  int hitHull;
  int hitHullD;
  int hitShield;
  int hitShieldD;
  int spentCharge;
  int spentChargeD;
  int spentForce;
  int spentForceD;
  int upgrade;
  int upgradeD;
  int extra;
  int extraD;
  // disabled functions
  int shipBGD;
};

// http://www.had2know.com/technology/rgb-to-gray-scale-converter.html
#define GETRGB(x) Color::GetColor(x).Red(), Color::GetColor(x).Green(), Color::GetColor(x).Blue()
#define GETGRY(x) Color::GetColor(x).Gray()*0.5, Color::GetColor(x).Gray()*0.5, Color::GetColor(x).Gray()*0.5

ColorPalette GetColorPalette(gdImagePtr &img) {
  ColorPalette colors;
  colors.clear        = gdImageColorAllocateAlpha(img,   0, 0, 0, 255);
  colors.bg           = gdImageColorAllocate(img,   0,   0,   0);
  colors.destroyed    = gdImageColorAllocateAlpha(img, 255, 0, 0, 192);
  colors.bgNone       = gdImageColorAllocate(img,  48,  48,  48);
  colors.bgNoneD      = gdImageColorAllocate(img,  24,  24,  24);
  colors.bgRebel      = gdImageColorAllocate(img,  65,   0,   0);
  colors.bgImperial   = gdImageColorAllocate(img,   0,  58,  19);
  colors.bgScum       = gdImageColorAllocate(img,  63,  51,   0);
  colors.white        = gdImageColorAllocate(img, 255, 255, 255);
  colors.black        = gdImageColorAllocate(img,   0,   0,   0);
  colors.attackDice   = gdImageColorAllocate(img, 192,   0,   0);
  colors.defenseDice  = gdImageColorAllocate(img,   0, 128,   0);
  colors.stress       = gdImageColorAllocate(img, GETRGB(Clr::Stress));
  colors.stressD      = gdImageColorAllocate(img, GETGRY(Clr::Stress));
  colors.neutral      = gdImageColorAllocate(img, GETRGB(Clr::Neutral));
  colors.neutralD     = gdImageColorAllocate(img, GETGRY(Clr::Neutral));
  colors.pilot        = gdImageColorAllocate(img, 255, 255, 255);
  colors.pilotD       = gdImageColorAllocate(img,  96,  96,  96);
  colors.cost         = gdImageColorAllocate(img, GETRGB(Clr::Cost));
  colors.costD        = gdImageColorAllocate(img, GETGRY(Clr::Cost));
  colors.initiative   = gdImageColorAllocate(img, GETRGB(Clr::Initiative));
  colors.initiativeD  = gdImageColorAllocate(img, GETGRY(Clr::Initiative));
  colors.attack       = gdImageColorAllocate(img, GETRGB(Clr::Attack));
  colors.attackD      = gdImageColorAllocate(img, GETGRY(Clr::Attack));
  colors.agility      = gdImageColorAllocate(img, GETRGB(Clr::Agility));
  colors.agilityD     = gdImageColorAllocate(img, GETGRY(Clr::Agility));
  colors.hull         = gdImageColorAllocate(img, GETRGB(Clr::Hull));
  colors.hullD        = gdImageColorAllocate(img, GETGRY(Clr::Hull));
  colors.shield       = gdImageColorAllocate(img, GETRGB(Clr::Shield));
  colors.shieldD      = gdImageColorAllocate(img, GETGRY(Clr::Shield));
  colors.charge       = gdImageColorAllocate(img, GETRGB(Clr::Charge));
  colors.chargeD      = gdImageColorAllocate(img, GETGRY(Clr::Charge));
  colors.force        = gdImageColorAllocate(img, GETRGB(Clr::Force));
  colors.forceD       = gdImageColorAllocate(img, GETGRY(Clr::Force));
  colors.hitHull      = gdImageColorAllocate(img,  61,  60,   6);
  colors.hitHullD     = gdImageColorAllocate(img,  54,  54,  54);
  colors.hitShield    = gdImageColorAllocate(img,  25,  59,  62);
  colors.hitShieldD   = gdImageColorAllocate(img,  49,  49,  49);
  colors.spentCharge  = gdImageColorAllocate(img, 110,  88,  16);
  colors.spentChargeD = gdImageColorAllocate(img,  63,  63,  63);
  colors.spentForce   = gdImageColorAllocate(img,  81,  65,  83);
  colors.spentForceD  = gdImageColorAllocate(img,  74,  74,  74);
  colors.upgrade      = gdImageColorAllocate(img, 255, 255, 255);
  colors.upgradeD     = gdImageColorAllocate(img,  96,  96,  96);
  colors.extra        = gdImageColorAllocate(img, 132, 106,  55);
  colors.extraD       = gdImageColorAllocate(img,  48,  48,  48);
  return colors;
}
#endif


static void DrawTitle(std::shared_ptr<ImageGenerator::ImageGen> ig, std::string title) {
  ImageGenerator::Box boxTitle = ImageGenerator::Box::FromTLWH(5, 560, 800, 50);
  ig->DrawRect(boxTitle, ImageGenerator::Color::BG());
  std::string titleString = title;
  double titleSize = 24.0;
  ig->DrawText(ImageGenerator::Font::Title(), titleSize, ImageGenerator::Align::Center, titleString, ImageGenerator::Color::WHITE(), IMG_WIDTH/2, 35);
}

static int DrawSquadTitle(std::shared_ptr<ImageGenerator::ImageGen> ig, Squad& squad, int xOffset, int yOffset, bool hasInit=0) {
  // title -> the entire area this function covers
  // glyph -> the faction icon (leftmost part of title)
  // name  -> the squad name (the rest of the title)
  // define the box that will contain everything
  ImageGenerator::Box bTitle = ImageGenerator::Box::FromTLWH(yOffset+5, xOffset+5, 370, 30);
  ig->DrawRect(bTitle, ImageGenerator::Color::BG());
  // prep the values
  std::string titleGlyph = IconGlyph::GetGlyph(squad.GetFac());
  std::string titleName = squad.GetName();
  double glyphSize = 18.0;
  double nameSize = 16.0;
  // draw the glyph
  int gx = xOffset+5;
  int gy = yOffset+5+21;
  ig->DrawText(ImageGenerator::Font::Icons(), glyphSize, ImageGenerator::Align::Left, titleGlyph, ImageGenerator::Color::WHITE(), gx, gy);
  // draw the title
  int tx = xOffset + 200;
  int ty = gy;
  ig->DrawText(ImageGenerator::Font::Title(), nameSize, ImageGenerator::Align::Center, titleName, ImageGenerator::Color::WHITE(), tx, ty);
  return bTitle.Height();
}



static int DrawPilot(std::shared_ptr<ImageGenerator::ImageGen> ig, Pilot& pilot, int xOffset, int yOffset) {
  // shorthand to see if pilot is enabled
  bool en = pilot.GS().IsEnabled();

  // figure out how tall some of the elements will be
  int actionXOffset  = 340;
  int actionYOffset  = 50;
  int actionHeight   = 30;
  int actionsHeight  = pilot.GetModActions().size() * actionHeight;
  int upgradeYOffset = 110;
  int upgradeHeight  = 25;
  int upgradesHeight = pilot.GetAppliedUpgrades().size() * upgradeHeight;
  int footer = 5;
  // first we need to draw a box for this pilot.
  // height will be mostly constant but will vary a bit based on the number of upgrades.
  // we can still pre-calculate it all given that we have fixed sizes for everything...

  // what is taller, pilot/stats/hitpoints/upgrades or actions?
  int pilotHeight  = std::max(upgradeYOffset+upgradesHeight-20, actionYOffset+actionsHeight-25);
  pilotHeight += footer;

  // background transparent image to darken background
  ImageGenerator::Box bPilot = ImageGenerator::Box::FromTLWH(yOffset, xOffset, SQUAD_WIDTH, pilotHeight);
  ig->DrawRect(bPilot, ImageGenerator::Color::BG());

  { // big ship icon
    std::string iconString = ShipGlyph::GetGlyph(pilot.GetShp());
    double iconSize = 96.0;
    int xPos = xOffset + 280;
    int yPos = yOffset +  90;
    ig->DrawText(ImageGenerator::Font::Ships(), iconSize, ImageGenerator::Align::Center, iconString, ImageGenerator::Color::BgNone(), xPos, yPos);
  }
  { // ship, limited, pilot, cost (across the top)
    double pilotFontSize = 20.0;
    double costFontSize = 20.0;
    std::string pilotString   = pilot.GetShortName();
    int yPos = yOffset+25;
    int xOff = xOffset;
    for(int i=0; i<pilot.GetLimited(); i++) {
      ig->DrawText(ImageGenerator::Font::Icons(), pilotFontSize, ImageGenerator::Align::Left, IconGlyph::GetGlyph(Ico::unique), ImageGenerator::Color::Pilot().GetDisabled(!en), xOff, yPos);
      xOff += 10;
    }
    ig->DrawText(ImageGenerator::Font::Title(), pilotFontSize, ImageGenerator::Align::Left,  pilotString, ImageGenerator::Color::Pilot().GetDisabled(!en), xOff, yPos);
    { // cost
      std::string costString = pilot.GetModCost() ? std::to_string(*pilot.GetModCost()) : "???";
      ig->DrawText(ImageGenerator::Font::Stats(), costFontSize, ImageGenerator::Align::Right, costString, ImageGenerator::Color::Cost().GetDisabled(!en), xOffset+SQUAD_WIDTH, yPos);
    }
  }
  { // actions
    double actionFontSize = 16.0;
    int xPos = xOffset + actionXOffset;
    int yPos = yOffset + actionYOffset;
    for(std::list<SAct> sa : pilot.GetModActions()) {
      if(sa.size() == 1) {
	// single action - just center it
	std::string glyph = IconGlyph::GetGlyph(sa.front().action);
	ig->DrawText(ImageGenerator::Font::Icons(), actionFontSize, ImageGenerator::Align::Center, glyph, (sa.front().difficulty == Dif::Red) ? ImageGenerator::Color::Stress().GetDisabled(!en) : ImageGenerator::Color::Neutral().GetDisabled(!en), xPos, yPos);
      }
      else if(sa.size() == 2) {
	// linked actions - center the '>' and snug the actions up to it
	// linked action
	std::string glyph = IconGlyph::GetGlyph(Ico::linked);
	ig->DrawText(ImageGenerator::Font::Icons(), actionFontSize, ImageGenerator::Align::Center, glyph, ImageGenerator::Color::Neutral().GetDisabled(!en), xPos, yPos);
	// first action
	glyph = IconGlyph::GetGlyph(sa.front().action);
	ig->DrawText(ImageGenerator::Font::Icons(), actionFontSize, ImageGenerator::Align::Right, glyph, (sa.front().difficulty == Dif::Red) ? ImageGenerator::Color::Stress().GetDisabled(!en) : ImageGenerator::Color::Neutral().GetDisabled(!en), xPos-8, yPos);
	// second action
	glyph = IconGlyph::GetGlyph(sa.back().action);
	ig->DrawText(ImageGenerator::Font::Icons(), actionFontSize, ImageGenerator::Align::Left, glyph, (sa.back().difficulty == Dif::Red) ? ImageGenerator::Color::Stress().GetDisabled(!en) : ImageGenerator::Color::Neutral().GetDisabled(!en), xPos+8, yPos);
      }
      else {
	printf("3 Linked Actions?\n");
      }
      yPos += actionHeight;
    }
  }
  { // stats
    double statsFontSize = 20.0;
    double chargeFontSize = 16.0;
    int space = 15;
    int xPos = xOffset +  0;
    int yPos = yOffset + 55;
    { // initiative
      std::string initString = std::to_string(pilot.GetModInitiative());
      xPos += 5 + space;
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, initString, ImageGenerator::Color::Initiative().GetDisabled(!en), xPos, yPos);
    }
    { // attack(s)
      for(PriAttack p : pilot.GetModAttacks()) {
        std::string arcString = IconGlyph::GetGlyph(p.arc);
	xPos += space + 25;
	ig->DrawText(ImageGenerator::Font::Icons(), statsFontSize, ImageGenerator::Align::Right, arcString, ImageGenerator::Color::Attack().GetDisabled(!en), xPos, yPos-3);
	std::string attackString = std::to_string(p.value);
	xPos += 15;
	ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, attackString, ImageGenerator::Color::Attack().GetDisabled(!en), xPos, yPos);
      }
    }
    { // agility
      std::string agilityString = std::to_string(pilot.GetModAgility());
      xPos += space + 15;
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, agilityString, ImageGenerator::Color::Agility().GetDisabled(!en), xPos, yPos);
    }
    { // hull
      std::string hullString = std::to_string(pilot.GetModHull());
      xPos += space + (hullString.size()*15);
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, hullString, ImageGenerator::Color::Hull().GetDisabled(!en), xPos, yPos);
    }
    if(pilot.GetModShield().GetCapacity()) { // shield
      std::string shieldString = std::to_string(pilot.GetModShield().GetCapacity());
      xPos += space + (shieldString.size()*15);
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, shieldString, ImageGenerator::Color::Shield().GetDisabled(!en), xPos, yPos);
    }
    if(pilot.GetModCharge().GetCapacity()) { // charge
      std::string chargeString = std::to_string(pilot.GetModCharge().GetCapacity());
      xPos += space + 15;
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, chargeString, ImageGenerator::Color::Charge().GetDisabled(!en), xPos, yPos);
      if(pilot.GetModCharge().GetRecurring()) {
	std::string recurringString;
	switch(pilot.GetModCharge().GetRecurring()) {
	case 1: recurringString = IconGlyph::GetGlyph(Ico::recurring); break;
	  //case 2: recurringString = IconGlyph::GetGlyph(Ico::recurring); break;
	}
	xPos += 10;
	ig->DrawText(ImageGenerator::Font::Icons(), statsFontSize, ImageGenerator::Align::Right, recurringString, ImageGenerator::Color::Charge().GetDisabled(!en), xPos, yPos);
      }
    }
    if(pilot.GetModForce().GetCapacity()) { // force
      std::string forceString = std::to_string(pilot.GetModForce().GetCapacity());
      xPos += space + 15;
      ig->DrawText(ImageGenerator::Font::Stats(), statsFontSize, ImageGenerator::Align::Right, forceString, ImageGenerator::Color::Force().GetDisabled(!en), xPos, yPos);
      if(pilot.GetModForce().GetRecurring()) {
	std::string recurringString;
	switch(pilot.GetModForce().GetRecurring()) {
	case 1: recurringString = IconGlyph::GetGlyph(Ico::recurring); break;
	  //case 2: recurringString = IconGlyph::GetGlyph(Ico::recurring); break;
	}
	xPos += 10;
	ig->DrawText(ImageGenerator::Font::Icons(), statsFontSize, ImageGenerator::Align::Right, recurringString, ImageGenerator::Color::Force().GetDisabled(!en), xPos, yPos);
      }
    }
    { // charge tokens
      if(pilot.GetModCharge().GetCapacity()) {
        std::string chargeString = IconGlyph::GetGlyph(Ico::charge);
        for(int i=0; i<pilot.GetModCharge().GetCapacity(); i++) {
	  xPos += space + 10;
	  ig->DrawText(ImageGenerator::Font::Icons(), chargeFontSize, ImageGenerator::Align::Right, chargeString,
		       (i < (pilot.GS().GetCurCharge())) ? ImageGenerator::Color::Charge().GetDisabled(!en) : ImageGenerator::Color::ChargeSpent().GetDisabled(!en),
		       xPos, yPos);
        }
      }
    }
    { // force tokens
      if(pilot.GetModForce().GetCapacity()) {
        std::string forceString = IconGlyph::GetGlyph(Ico::forcepower);
        for(int i=0; i<pilot.GetModForce().GetCapacity(); i++) {
	  xPos += space + 10;
	  ig->DrawText(ImageGenerator::Font::Icons(), chargeFontSize, ImageGenerator::Align::Right, forceString,
		       (i < (pilot.GS().GetCurForce())) ? ImageGenerator::Color::Force().GetDisabled(!en) : ImageGenerator::Color::ForceSpent().GetDisabled(!en),
		       xPos, yPos);
        }
      }
    }
  }
  { // hull/shields
    int xPos = xOffset +  2;
    int yPos = yOffset + 80;

    int xSize = 20;
    float hpFontSize = 16.0;
    {
      int hp = pilot.GetModHull()+pilot.GetModShield().GetCapacity();
      if(hp >= 20) {
	xSize = 10;
	hpFontSize = 8.0;
      }
      else if(hp >= 12) {
	xSize = 18;
	hpFontSize = 14.0;
      }
    }

    std::string hullString = IconGlyph::GetGlyph(Ico::hull);
    std::string shieldString = IconGlyph::GetGlyph(Ico::shield);
    for(int i=0; i<pilot.GetModHull(); i++) {
      ig->DrawText(ImageGenerator::Font::Icons(), hpFontSize, ImageGenerator::Align::Left, hullString,
		   (i < (pilot.GS().GetCurHull())) ? ImageGenerator::Color::Hull().GetDisabled(!en) : ImageGenerator::Color::HullSpent().GetDisabled(!en),
		   xPos, yPos);
      xPos += xSize;
    }
    for(int i=0; i<pilot.GetModShield().GetCapacity(); i++) {
      ig->DrawText(ImageGenerator::Font::Icons(), hpFontSize, ImageGenerator::Align::Left, shieldString,
		   (i < (pilot.GS().GetCurShield())) ? ImageGenerator::Color::Shield().GetDisabled(!en) : ImageGenerator::Color::ShieldSpent().GetDisabled(!en),
		   xPos, yPos);
      xPos += xSize;
    }
#ifdef USE_DASHES
    // current shield/hull
    int yHp = 60;
    int hpWidth = 280;
    int segments = pilot.GetModShield() + pilot.GetModHull();
    int segWidth =  hpWidth / segments;
    for(int i=0; i<pilot.GetModHull(); i++) {
      Box dummyBox = Box::FromTLWH(yHp+yOffset, (segWidth*i)+10+2, segWidth-4, 10);
      gdImageFilledRectangle(img, xOffset+dummyBox.Left(), dummyBox.Top(), xOffset+dummyBox.Right(), dummyBox.Bottom(),
			     (i < (pilot.GS().GetCurHull())) ? en?colors.hull:colors.hullD : en?colors.hitHull:colors.hitHullD);
    }
    for(int i=0; i<pilot.GetModShield(); i++) {
      Box dummyBox = Box::FromTLWH(yHp+yOffset, (pilot.GetModHull()*segWidth)+(segWidth*i)+10+2, segWidth-4, 10);
      gdImageFilledRectangle(img, xOffset+dummyBox.Left(), dummyBox.Top(), xOffset+dummyBox.Right(), dummyBox.Bottom(),
			     (i < (pilot.GS().GetCurShield())) ? en?colors.shield:colors.shieldD : en?colors.hitShield:colors.hitShieldD);
    }
#endif
  }
  { // upgrades
    int upgradeFontSize = 16.0;
    //int uCount = 0;
    int xOff = 0;
    int yOff = yOffset + upgradeYOffset;
    for(auto u : pilot.GetAppliedUpgrades()) {
      xOff = xOffset;
      // draw the icon
      std::string glyph = IconGlyph::GetGlyph(u.GetUpT());
      ig->DrawText(ImageGenerator::Font::Icons(), upgradeFontSize, ImageGenerator::Align::Left, glyph, ImageGenerator::Color::Upgrade().GetDisabled(!en), xOff, yOff);
      xOff += 25;
      // draw the limited dots
      for(int i=0; i<u.GetLimited(); i++) {
	glyph = IconGlyph::GetGlyph(Ico::unique);
	ig->DrawText(ImageGenerator::Font::Icons(), upgradeFontSize, ImageGenerator::Align::Left, glyph, ImageGenerator::Color::Upgrade().GetDisabled(!en), xOff, yOff);
        xOff += 7;
      }
      // draw the upgrade name
      glyph = u.GetShortTitle();
      ig->DrawText(ImageGenerator::Font::Title(), upgradeFontSize, ImageGenerator::Align::Left, glyph, ImageGenerator::Color::Upgrade().GetDisabled(!en), xOff, yOff);
      // guess how far over to draw the charges
      int estWidth = glyph.size() * upgradeFontSize * 0.95;
      xOff += estWidth;
      // draw the carge icons
      if(u.GetCharge().GetCapacity()) {
        glyph = IconGlyph::GetGlyph(Ico::charge);
        for(int i=0; i<u.GetCharge().GetCapacity(); i++) {
	  ig->DrawText(ImageGenerator::Font::Icons(), upgradeFontSize, ImageGenerator::Align::Left, glyph,
		       (i < (u.GS().GetCurCharge())) ? ImageGenerator::Color::Charge().GetDisabled(!en) : ImageGenerator::Color::ChargeSpent().GetDisabled(!en),
		       xOff, yOff);
          xOff += 20;
        }
      }
      yOff += upgradeHeight;
    }
  }
  // cross out if disabled and hull is zero
  if(!en && !pilot.GS().GetCurHull()) {
    ig->DrawLine(ImageGenerator::Color::Destroyed(), 5, bPilot.Left(), bPilot.Top(),     bPilot.Right(), bPilot.Bottom());
    ig->DrawLine(ImageGenerator::Color::Destroyed(), 5, bPilot.Left(), bPilot.Bottom(),  bPilot.Right(), bPilot.Top());
    
  }
  return pilotHeight;
}



static void DrawSquad(std::shared_ptr<ImageGenerator::ImageGen> ig, Squad &squad, uint8_t initiative, int xOffset, int yOffset/*, ColorPalette const &colors*/) {
  yOffset += DrawSquadTitle(ig, squad, xOffset, yOffset, initiative==1 ? 1 : 0);
  yOffset += 10;
  for(auto& pilot : squad.GetPilots()) {
    yOffset += DrawPilot(ig, pilot, xOffset, yOffset);
    yOffset += 5;  // some space between pilots
  }
}



std::optional<int16_t> GetDefeatedPoints(Squad squad) {
  int16_t ret = 0;
  bool hasLiving = false;
  for(Pilot &p : squad.GetPilots()) {
    if(p.GS().GetCurHull() == 0) {
      if(p.GetModCost()) {
	ret += *p.GetModCost();
      } else {
	return std::nullopt;
      }
    } else {
      hasLiving = true;
      uint8_t totalHp = p.GetModHull() + p.GetModShield().GetCapacity();
      uint8_t halfHp = totalHp / 2;
      if((p.GS().GetCurHull() + p.GS().GetCurShield()) <= halfHp) {
	if(p.GetModCost()) {
	  ret += *p.GetModCost() / 2;
	} else {
	  return std::nullopt;
	}
      }
    }
  }
  if(!hasLiving) { ret = 200; }
  return ret;
}

bool OnlyNpRemains(Squad s) {
  /*
  if(s.GetFaction().GetType() != Fac::Scum) { return false; }
  for(Pilot &p : s.GetPilots()) {
    if(p.GetXws() == "nashtahpuppilot") {
      if(!p.IsEnabled()) { return false; }
    }
    else {
      if(p.IsEnabled()) { return false; }
    }
  }
  */
  return false;
}

std::pair<std::optional<int16_t>, std::optional<int16_t>> GetScore(Squad s1, Squad s2) {
  std::optional<int16_t> p1lost = GetDefeatedPoints(s1);
  std::optional<int16_t> p2lost = GetDefeatedPoints(s2);
  // stupid nashtah pup...
  if     (OnlyNpRemains(s1) && p2lost==100) { return {101, 99};        }
  else if(OnlyNpRemains(s2) && p1lost==100) { return {99, 101};        }
  else                                      { return {p2lost, p1lost}; }
}

static void DrawScore(std::shared_ptr<ImageGenerator::ImageGen> ig, bool p1, std::optional<int16_t> value) {
  std::string score = value ? std::to_string(*value) : "???";
  double      size  = 36.0;
  int bw = 100;
  int bh =  50;
  ImageGenerator::Box bScore = ImageGenerator::Box::FromTLWH(5, p1 ? SQUAD_WIDTH+5 : IMG_WIDTH-SQUAD_WIDTH-bw-5, bw, bh);
  ig->DrawRect(bScore, ImageGenerator::Color::BG());
  ig->DrawText(ImageGenerator::Font::Stats(), size, ImageGenerator::Align::Right, score, ImageGenerator::Color::WHITE(), bScore.Right(), bScore.Bottom()-5);
}


/*
static void DrawDice(gdImagePtr img, Dice& dice, int xOffset, int yOffset, ColorPalette const &colors) {
  int x = xOffset;
  int y = yOffset;

  if(dice.state == Dice::State::None) { return; }

  else if(dice.state == Dice::State::Attacking) {
    for(auto ad : dice.attackDice.GetSortedDice()) {
      Box boxTitle = Box::FromTLWH(y, x, 100, 100);
      gdImageFilledRectangle(img, boxTitle.Left(), boxTitle.Top(), boxTitle.Right(), boxTitle.Bottom(), colors.attackDice);
      Box bsIcon = DrawText(IconGlyph::GetGlyph(ad.GetType()).c_str(), iconsFont, 40, img, colors.white, 0, 0);
      DrawText(IconGlyph::GetGlyph(ad.GetType()).c_str(), iconsFont, 40, img, colors.white, x+(boxTitle.Width()-bsIcon.Width())/2, y+15+(boxTitle.Height())/2);
      y += boxTitle.Height() + 10;
    }
  }

  else if(dice.state == Dice::State::Defending) {
    for(auto dd : dice.defenseDice.GetSortedDice()) {
      Box boxTitle = Box::FromTLWH(y, x, 100, 100);
      gdImageFilledRectangle(img, boxTitle.Left(), boxTitle.Top(), boxTitle.Right(), boxTitle.Bottom(), colors.defenseDice);
      Box bsIcon = DrawText(IconGlyph::GetGlyph(dd.GetType()).c_str(), iconsFont, 40, img, colors.white, 0, 0);
      DrawText(IconGlyph::GetGlyph(dd.GetType()).c_str(), iconsFont, 40, img, colors.white, x+(boxTitle.Width()-bsIcon.Width())/2, y+15+(boxTitle.Height())/2);
      y += boxTitle.Height() + 10;
    }
  }
}
*/


//
// this one generates an image of a single squad
//
void GenerateImage(Squad& squad, std::string name) {
  // this will get redone
  std::shared_ptr<ImageGenerator::ImageGen> ig = std::make_shared<ImageGenerator::ImageGen_GD>(SQUAD_WIDTH, SQUAD_HEIGHT);
  ig->Init();
  DrawSquad(ig, squad, 0, 0, 0);
  ig->Finish();
  ig->Save(name);
}


//
// this one generates the overay image of both squads
//
void GenerateImage(ImageGenToken token) {
  // generate one large image with both squads
  if(token.settings.GetSetting("run.multiimage") == "no") {
    // this will get redone
    std::shared_ptr<ImageGenerator::ImageGen> ig = std::make_shared<ImageGenerator::ImageGen_GD>(IMG_WIDTH, IMG_HEIGHT);
    ig->Init();

    // title
    if(token.settings.GetSetting("run.title") != "") {
      DrawTitle(ig, token.settings.GetSetting("run.title"));
    }

    std::pair<std::optional<int16_t>, std::optional<int16_t>> score = GetScore(token.p1squad, token.p2squad);

    // P1
    DrawSquad(ig, token.p1squad, token.initiative==1?1:0, 0, 0);
    DrawScore(ig, true, score.first);
    //DrawDice(img, token.dice[0], SQUAD_WIDTH+5, 50, colors);

    // P2
    DrawSquad(ig, token.p2squad, token.initiative==2?1:0, IMG_WIDTH-SQUAD_WIDTH, 0);
    DrawScore(ig, false, score.second);
    //DrawDice(img, token.dice[1], IMG_WIDTH-SQUAD_WIDTH-105, 50, colors);

    // save the image
    ig->Finish();
    std::string file = token.settings.GetSetting("run.outdir");
    if(file[file.length()-1] != '/') { file += '/'; }
    file += "xhud.png";
    ig->Save(file);
  }
  // generate a separate image for each squad
  else {
    std::string rootDir = token.settings.GetSetting("run.outdir");
    if(rootDir[rootDir.length()-1] != '/') {
      rootDir += '/';
    }
    for(int i=1; i<=2; i++) {
      std::string filename = rootDir+"p"+ std::to_string(i) + ".png";
      // create the image
      std::shared_ptr<ImageGenerator::ImageGen> ig = std::make_shared<ImageGenerator::ImageGen_GD>(SQUAD_WIDTH, SQUAD_HEIGHT);
      ig->Init();
      // squad
      DrawSquad(ig, (i==1) ? token.p1squad : token.p2squad, token.initiative==i?1:0, 0, 0);
      ig->Finish();
      ig->Save(filename);
    }
  }
}



std::string GetImageGenInfo(std::string imgGenFmt) {
  std::shared_ptr<ImageGenerator::ImageGen> ig = GetImageGen(imgGenFmt,1,1);
  return ig->GetInfo();
}
