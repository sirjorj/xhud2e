#include "imagegen_base.h"
#include <iomanip>
#include <sstream>

namespace ImageGenerator {

//
// box
//
Box Box::FromTLBR(int top, int left, int bottom, int right) {
  return Box(top, left, right-left+1, bottom-top+1);
}

Box Box::FromTLWH(int top, int left, int width, int height) {
  return Box(top, left, width, height);
}

int Box::Top()    { return this->top; }
int Box::Left()   { return this->left; }
int Box::Width()  { return this->width; }
int Box::Height() { return this->height; }
int Box::Bottom() { return this->height + this->top - 1; }
int Box::Right()  { return this->width + this->left - 1; }
Box Box::MoveTL(int top, int left) { return Box(top, left, this->width, this->height); }
void Box::Dump()  { printf("top:%d left:%d width:%d height:%d\n", this->top, this->left, this->width, this->height); }

Box::Box() { }
Box::Box(int t, int l, int w, int h) : top(t), left(l), width(w), height(h) { }



//
// color
//
uint8_t Color::GetRed()   const { return this->red;   }
uint8_t Color::GetGreen() const { return this->green; }
uint8_t Color::GetBlue()  const { return this->blue;  }
uint8_t Color::GetAlpha() const { return this->alpha; }

// https://www.tutorialspoint.com/dip/grayscale_to_rgb_conversion.htm
// ( (0.3 * R) + (0.59 * G) + (0.11 * B) )
Color Color::GetDisabled(bool disabled) {
  if(disabled) {
    double g = ((0.3 * this->GetRed()) + (0.59 * this->GetGreen()) + (0.11 * this->GetBlue()));
    uint8_t val = (uint8_t) (g+0.5);
    return Color(val, val, val, 255);
  } else {
    return *this;
  }
}

std::string Color::GetHexA() const {
  std::stringstream ss;
  ss << std::hex
     << std::setfill('0') << std::setw(2) << int(this->GetRed())
     << std::setfill('0') << std::setw(2) << int(this->GetGreen())
     << std::setfill('0') << std::setw(2) << int(this->GetBlue())
     << std::setfill('0') << std::setw(2) << int(this->GetAlpha());
  return ss.str();
}

Color::Color(uint8_t r, uint8_t g, uint8_t b)            : red(r), green(g), blue(b), alpha(255) {}
Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : red(r), green(g), blue(b), alpha(a) {}



//
// font
//
std::string Font::GetName() const { return this->name; }
std::string Font::GetFile() const { return this->file; }

Font::Font(std::string n, std::string f) : name(n), file(f) { }



//
// ImageGen (base)
//
ImageGen::ImageGen(int w, int h) : width(w), height(h) { }
}
