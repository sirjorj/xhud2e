#pragma once
#include "imagegen_base.h"
#include <gd.h>
#include <map>

namespace ImageGenerator {

class ImageGen_GD : public ImageGen {
 public:
  ImageGen_GD(int width, int height);
  virtual ~ImageGen_GD();
  virtual void Init() override final;
  virtual void DrawLine(Color c, int t, int x1, int y1, int x2, int y2) override final;
  virtual void DrawRect(Box b, Color c) override final;
  virtual void DrawText(Font f, double s, Align a, std::string t, Color c, int x, int y) override final;
  virtual void Finish() override final;
  virtual void Save(std::string f) override final;
  virtual std::string GetInfo() override final;
 private:
  Box GetTextSize(Font f, double s, std::string t);
  int GetGdColor(const Color& c);
  std::map<std::string,int> colorPalette;
  gdImagePtr gdImg;
};

}
