#pragma once
#include <libxwing2/color.h>
#include <string>

namespace ImageGenerator {

class Box {
public:
  static Box FromTLBR(int top, int left, int bottom, int right);
  static Box FromTLWH(int top, int left, int width, int height);
  int Top();
  int Left();
  int Width();
  int Height();
  int Bottom();
  int Right();
  Box MoveTL(int top, int left);
  void Dump();
private:
  Box();
  Box(int t, int l, int w, int h);
  int top, left, width, height;
};



#define GETRGB(x) libxwing2::Color::GetColor(x).Red(), libxwing2::Color::GetColor(x).Green(), libxwing2::Color::GetColor(x).Blue()
#define GETGRY(x) Color::GetColor(x).Gray()*0.5, Color::GetColor(x).Gray()*0.5, Color::GetColor(x).Gray()*0.5
class Color {
 public:
  static Color Clear()       { return Color(  0,  0,  0,  0); }
  static Color BG()          { return Color(  0,  0,  0,255); }
  static Color Destroyed()   { return Color(255,  0,  0,192); }

  static Color Pilot()       { return Color(255,255,255,255); }
  static Color Cost()        { return Color(GETRGB(libxwing2::Clr::Cost)); }
  static Color Initiative()  { return Color(GETRGB(libxwing2::Clr::Initiative)); }
  static Color Attack()      { return Color(GETRGB(libxwing2::Clr::Attack)); }
  static Color Agility()     { return Color(GETRGB(libxwing2::Clr::Agility)); }
  static Color Hull()        { return Color(GETRGB(libxwing2::Clr::Hull)); }
  static Color HullSpent()   { return Color( 61, 60,  6,255); }
  static Color Shield()      { return Color(GETRGB(libxwing2::Clr::Shield)); }
  static Color ShieldSpent() { return Color( 25, 59, 62,255); }
  static Color Charge()      { return Color(GETRGB(libxwing2::Clr::Charge)); }
  static Color ChargeSpent() { return Color(110, 88, 16,255); }
  static Color Force()       { return Color(GETRGB(libxwing2::Clr::Force)); }
  static Color ForceSpent()  { return Color( 81, 65, 83,255); }

  static Color Upgrade()     { return Color(255,255,255,255); }
  
  static Color Stress()    { return Color(GETRGB(libxwing2::Clr::Stress)); }
  static Color Neutral()   { return Color(GETRGB(libxwing2::Clr::Neutral)); }
  
  static Color BgNone()    { return Color( 48, 48, 48,255); }
  
  static Color WHITE()     { return Color(255,255,255,255); }
  static Color TEST()      { return Color(0,0,255,255); }

  Color GetDisabled(bool disabled=true);

  uint8_t GetRed()      const;
  uint8_t GetGreen()    const;
  uint8_t GetBlue()     const;
  uint8_t GetAlpha()    const; // 0 is clear, 255 is opaque
  std::string GetHexA() const;
 private:
  uint8_t red;
  uint8_t green;
  uint8_t blue;
  uint8_t alpha;
  Color(uint8_t r, uint8_t g, uint8_t b);
  Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
};



class Font {
 public:
  static Font Icons() { return Font("xwing-miniatures",       "./fonts/xwing-miniatures.ttf");        }
  static Font Ships() { return Font("xwing-miniatures-ships", "./fonts/xwing-miniatures-ships.ttf");  }
  static Font Title() { return Font("Squarish Sans",          "./fonts/SquarishSansCTRegularSC.ttf"); }
  static Font Stats() { return Font("xwstats",                "./fonts/xwstats.ttf");                 }
  std::string GetName() const;
  std::string GetFile() const;
 private:
  std::string name;
  std::string file;
  Font(std::string n, std::string f);
};



enum class Align { Left, Center, Right };



class ImageGen {
 public:
  ImageGen(int width, int height);
  virtual void Init() = 0;
  virtual void DrawLine(Color c, int t, int x1, int y1, int x2, int y2) = 0;
  virtual void DrawRect(Box b, Color c) = 0;
  virtual void DrawText(Font f, double s, Align a, std::string t, Color c, int x, int y) = 0;
  virtual void Finish() = 0;
  virtual void Save(std::string f) = 0;
  virtual std::string GetInfo() = 0;
 protected:
  int width;
  int height;
};

}
