#include "game.h"
#include "colors.h"
#include "helpers.h"
#include "printhelpers.h"
#include <iostream>

// https://regex101.com
// remember: ignore the R"()"
// options
const std::regex RX_QUIT(R"(^qqq$)");                                   // quit
const std::regex RX_HELP(R"(^[?]$)");                                   // help
// settings
const std::regex RX_SETP(R"(^settings$)");                                                 // print all current settings
const std::regex RX_SETR(R"(^settings reload( [[:alnum:].]+)?$)");                         // reload setting(s) from config
const std::regex RX_SETW(R"(^settings write$)");                                           // write setting(s) to config
const std::regex RX_SETS(R"(^settings [[:alnum:].]+(=[[:alnum:][:space:][:punct:]]*)?$)"); // set key (or print value if no 'val')
// squads
const std::regex RX_PLT( R"(^[12][1-8][SsHhAaCcFf]+$)");                // pilot
const std::regex RX_UPG( R"(^[12][1-8][1-9]([AaFfCc]+|\([a-z]*\))+$)"); // upgrade
// global
const std::regex RX_EP(R"(^ep$)");                                      // end phase
//const std::regex RX_OVR( R"(^[12][1-8]((O(sk|at|ag|hu|sh)[0-9]{1,2})|(o(sk|at|ag|hu|sh)))$)"); // override  



Game::Game(std::array<Squad, 2>& p, SettingsManager &s)
  : token{s, p[0], p[1]}, players(p), isRunning(true) {
}

void Game::Setup() {
  for(Squad &s : this->players) {
    for(Pilot &p : s.GetPilots()) {
      for(Upgrade &u : p.GetAppliedUpgrades()) {
	u.Setup(u);
      }
    }
  }
}

void Game::Run() {
  GenerateImage(token);//this->players[0], this->players[1], this->outFile);
  this->Setup();
  do {
    std::string line;
    printf("%sxhud> ", CLR_GPT);
    std::getline(std::cin, line);
    if(this->ParseCommand(line)) {
      GenerateImage(token);//this->players[0], this->players[1], this->outFile);
    }
  } while(this->isRunning);
}


// return is whether or not to redraw the images
bool Game::ParseCommand(std::string command) {

  if(command == "") {
    printf("%s  Title: '%s'\n", CLR_GAM, this->token.settings.GetSetting("run.title").c_str());
    for(uint8_t player : {1,2}) {
      printf("%s  Player %hhu%s%s\n", CLR_GAM, player, CLR_LBL, this->token.initiative==player ? " [INITIATIVE]" : "");
      uint8_t pc = 1;
      uint8_t nameLen = 0;
      for(auto p : this->players[player-1].GetPilots()) {
	if(p.GetName().length() > nameLen) { nameLen = p.GetName().length(); }
	for(auto u : p.GetAppliedUpgrades()) {
	  if(u.GetName().length()+2 > nameLen) { nameLen = u.GetName().length()+2; }
	}
      }
      for(auto p : this->players[player-1].GetPilots()) {
	printf("  %s%hhu %s%hhu%s) %s%-*s", CLR_GAM, player, CLR_PLT, pc, CLR_LBL, CLR_PLT, nameLen, p.GetName().c_str());
	printf(" %s[%s%s%s]%s", CLR_PLT,  p.GS().IsEnabled() ? CLR_ENA : CLR_DIS, p.GS().IsEnabled() ? "A" : "a", CLR_PLT, CLR_GAM);
	printf(" %s[", CLR_PLT);
	for(int i=0; i<p.GS().GetCurHull();    i++) { printf("%sH", CLR_HUL); }
	for(int i=0; i<p.GS().GetHullHits();   i++) { printf("%s.", CLR_HUL); }
	for(int i=0; i<p.GS().GetCurShield();  i++) { printf("%sS", CLR_SHD); }
	for(int i=0; i<p.GS().GetShieldHits(); i++) { printf("%s.", CLR_SHD); }
	printf("%s]%s", CLR_PLT, CLR_GAM);
	if(p.GetModCharge().GetCapacity()) {
	  printf(" %s[", CLR_PLT);
	  for(int i=0; i<p.GS().GetCurCharge();  i++) { printf("%sC", CLR_CHG); }
	  for(int i=0; i<(p.GetModCharge().GetCapacity() - p.GS().GetCurCharge());   i++) { printf("%s.", CLR_CHG); }
	  printf("%s]%s", CLR_PLT, CLR_GAM);
	}
	if(p.GetModForce().GetCapacity()) {
	  printf(" %s[", CLR_PLT);
	  for(int i=0; i<p.GS().GetCurForce();  i++) { printf("%sF", CLR_FRC); }
	  for(int i=0; i<(p.GetModForce().GetCapacity() - p.GS().GetCurForce());   i++) { printf("%s.", CLR_FRC); }
	  printf("%s]%s", CLR_PLT, CLR_GAM);
	}
	printf(" %s%2d%s", CLR_INI, p.GetModInitiative(), CLR_GAM);
	for(PriAttack a : p.GetNatAttacks()) {
	  printf(" %s%s%d", CLR_ATK, GetArc(a.arc).c_str(), a.value);
	}
	printf(" %s%d%s", CLR_AGI, p.GetModAgility(), CLR_GAM);
	printf(" %s%d%s", CLR_HUL, p.GetModHull(), CLR_GAM);
	printf(" %s%d%s", CLR_SHD, p.GetModShield().GetCapacity(), CLR_GAM);
	if(p.GetModCharge().GetCapacity()) {
	  printf(" %s%d%s%s", CLR_CHG, p.GetModCharge().GetCapacity(), p.GetModCharge().GetRecurring() ? "^" : "", CLR_GAM);
	}
	if(p.GetModForce().GetCapacity()) {
	  printf(" %s%d%s%s", CLR_FRC, p.GetModForce().GetCapacity(), p.GetModForce().GetRecurring() ? "^" : "", CLR_GAM);
	}
	printf("\n");
	uint8_t uc = 1;
	for(auto u : p.GetAppliedUpgrades()) {
	  printf("  %s%hhu %s%d %s%hhu%s) %s%-*s", CLR_GAM, player, CLR_PLT, pc, CLR_UPG, uc++, CLR_LBL, CLR_UPG, nameLen-2, u.GetName().c_str());
	  printf(" %s[%s%s%s]%s", CLR_UPT, u.GS().IsEnabled() ? CLR_ENA : CLR_DIS, u.GS().IsEnabled() ? "A" : "a", CLR_UPT, CLR_GAM);
	  printf("%s", CLR_ENA);
	  if(u.GetCharge().GetCapacity()) {
	    printf(" %s[", CLR_UPT);
	    for(int i=0; i<u.GS().GetCurCharge();  i++) { printf("%sC", CLR_CHG); }
	    for(int i=0; i<(u.GetCharge().GetCapacity() - u.GS().GetCurCharge());   i++) { printf("%s.", CLR_CHG); }
	    printf("%s]%s", CLR_UPT, CLR_GAM);
	  }
	  printf("%s\n", CLR_GAM);
	}
	pc++;
      }
    }
    printf("\n");
  }

  for(auto cmd : tokenize(command, ',')) {

    // quit
    if(std::regex_match(cmd, RX_QUIT)) {
      printf("%sQuit!\n", CLR_GAM);
      this->isRunning = false;
    }

    // help
    else if(std::regex_match(cmd, RX_HELP)) {
      printf("%s", CLR_GAM);
      printf("Commands:\n");
      printf("[Options]\n");
      printf("  ?      - help\n");
      printf("  qqq    - quit\n");
      printf("         - print current status (yes, that's a blank line)\n");
      printf("[Settings]\n");
      printf("  settings         - print the current settings\n");
      printf("  settings {k}={v} - set setting {k} to {v}\n");
      printf("  settings write   - write the current settings to the config file\n");
      printf("[Game State]\n");
      printf("  ep     - handle End Phase triggers (+1 all recurring charges)");
      //printf("  i#     - set player '#' as having initiative (0 to disable)\n");
      printf("  <PSC>  - modify ship stats\n");
      printf("  <PSUC> - modify upgrade status\n");
      printf("   P     - player number (1 or 2)\n");
      printf("   S     - ship number (1..n counting down)\n");
      printf("   U     - upgrade number (1..n left to right, top to bottom)\n");
      printf("   C     - command(s)\n");
      printf("     s     - shield down\n");
      printf("     S     - shield up\n");
      printf("     h     - hull down\n");
      printf("     H     - hull up\n");
      printf("     a     - inactive\n");
      printf("     A     - active\n");
      printf("     c     - remove standard charge\n");
      printf("     C     - add standard charge\n");
      printf("     f     - remove force charge\n");
      printf("     F     - add force charge\n");
      //printf("     oxx   - disable override (where 'xx' is sk, at, ag, hu, sh)\n");
      //printf("     Oxxv  - overrides 'xx' (where 'xx' is sk, at, ag, hu, sh) to value 'v'\n");
      //printf("[Dice]\n");
      //printf("  <PDR>   - Print dice results where:\n");
      //printf("   P - player (1 or 2))\n");
      //printf("   D - die type ('a' for attack, 'd' for defense)\n");
      //printf("   R - 1 or more results where:\n");
      //printf("     c - critical hit\n");
      //printf("     h - hit\n");
      //printf("     f - focus\n");
      //printf("     b - blank\n");
      //printf("     e - evade\n");
      //printf("  c  - clear the dice results\n");
      //printf("[Data Lookup]\n");
      //printf("  p n     - search for pilot 'n' and show its info\n");
      //printf("  p n f s - show info for pilot specified by name/faction/ship\n");
      //printf("  u n     - search for upgrade 'n' and show its info\n");
      //printf("  u t n   - show info for upgrade specified by upgradetype/upgradename\n");
      printf("\n");
      printf("Examples:\n");
      printf("    i1       - player 1 has initiative\n");
      printf("    13s      - player 1, ship 3 loses a shield\n");
      printf("    21ssh    - player 2, ship 1 loses 2 shield and a hull\n");
      printf("    11hha    - player 1, ship 1 loses 2 hull and is disabled\n");
      printf("    231a     - player 2, ship 3, upgrade 1 is disabled\n");
      printf("    11h, 12h - player 1, ships 1 and 2 each lose a hull\n");
      //printf("    1ahhf    - player 1 rolls 2 hits and a focus\n");
      //printf("    2debb    - player 2 rolls an evade and 2 blanks\n");
      //printf("    c        - clear the dice results\n");
      //printf("    settings run.title=My Game - Sets title to \"My Game\"\n");
    }


    // print all settings
    else if(std::regex_match(cmd, RX_SETP)) {
      token.settings.Dump();
    }

    // reload settings
    //else if(std::regex_match(cmd, RX_SETR)) {
    //  printf("Reload Settings not yet implemented\n");
    //}

    // write settings
    else if(std::regex_match(cmd, RX_SETW)) {
      token.settings.WriteSettings();
      printf("Settings written to config\n");
    }

    // set a setting
    else if(std::regex_match(cmd, RX_SETS)) {
      std::string str = cmd.substr(9); // skip "settings "
      std::string key = str.substr(0,str.find('='));
      std::optional<std::string> val;
      if(str.find('=') != std::string::npos) {
	val = str.substr(str.find('=')+1);
      }
      //printf("str -> %s\n", str.c_str());
      //printf("key -> %s\n", key.c_str());
      //printf("val -> %s\n", val ? val->c_str() : "*null*");

      try {
	if(val) {
	  token.settings.SetSetting(key, *val);
	}
	else {
	  val = token.settings.GetSetting(key);
	}
	if(val) {
	  printf("  %s%s %s-> %s%s\n", CLR_KEY, key.c_str(), CLR_DEF, CLR_VAL, val->c_str());
	}
      }
      catch(SettingNotFound &snf) {
	printf("  %sInvalid key %s%s\n", CLR_ERR, CLR_KEY, key.c_str());
      }
    }

    // pilot
    else if(std::regex_match(cmd, RX_PLT)) {
      uint8_t player = cmd[0] - 48; // verified by regex
      uint8_t ship   = cmd[1] - 48; // needs bounds checking
      if((ship < 1) || ( ship > this->players[player-1].GetPilots().size())) {
	printf("%sInvalid Ship\n", CLR_ERR);
	continue;
      }
      std::string pn = this->players[player-1].GetPilots()[ship-1].GetName();
      for(auto c : cmd.substr(2)) {
	switch(c) {
	case 's': this->players[player-1].GetPilots()[ship-1].GS().ShieldDn(); printf("%s  Player %d - Pilot %d (%s%s%s) - Shield Down\n", CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'S': this->players[player-1].GetPilots()[ship-1].GS().ShieldUp(); printf("%s  Player %d - Pilot %d (%s%s%s) - Shield Up\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'h': this->players[player-1].GetPilots()[ship-1].GS().HullDn();   printf("%s  Player %d - Pilot %d (%s%s%s) - Hull Down\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM);
	  if(token.settings.GetSetting("run.autodisable") == "yes") {
	    if(this->players[player-1].GetPilots()[ship-1].GS().GetCurHull() == 0) {
	      this->players[player-1].GetPilots()[ship-1].GS().Disable();
	      printf("%s  Player %d - Pilot %d (%s%s%s) - Inactive\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM);
	    }
	  }
	  break;
	case 'H': this->players[player-1].GetPilots()[ship-1].GS().HullUp();   printf("%s  Player %d - Pilot %d (%s%s%s) - Hull Up\n",     CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'a': this->players[player-1].GetPilots()[ship-1].GS().Disable();  printf("%s  Player %d - Pilot %d (%s%s%s) - Inactive\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'A': this->players[player-1].GetPilots()[ship-1].GS().Enable();   printf("%s  Player %d - Pilot %d (%s%s%s) - Active\n",      CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'c': this->players[player-1].GetPilots()[ship-1].GS().ChargeDn(); printf("%s  Player %d - Pilot %d (%s%s%s) - Charge Down\n", CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'C': this->players[player-1].GetPilots()[ship-1].GS().ChargeUp(); printf("%s  Player %d - Pilot %d (%s%s%s) - Charge Up\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'f': this->players[player-1].GetPilots()[ship-1].GS().ForceDn();  printf("%s  Player %d - Pilot %d (%s%s%s) - Force Down\n",  CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	case 'F': this->players[player-1].GetPilots()[ship-1].GS().ForceUp();  printf("%s  Player %d - Pilot %d (%s%s%s) - Force Up\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
	}
      }
    }

    // upgrade
    else if(std::regex_match(cmd, RX_UPG)) {
      uint8_t player = cmd[0] - 48; // verified by regex
      uint8_t ship   = cmd[1] - 48; // needs bounds checking
      uint8_t upg    = cmd[2] - 48; // needs bounds checking
      if(ship > this->players[player-1].GetPilots().size()) {
	printf("%sInvalid Ship\n", CLR_ERR);
	continue;
      }
      if((upg < 1) || (upg > this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades().size()+1)) {
	printf("%sInvalid upgrade\n", CLR_ERR);
	continue;
      }
      std::string pn = this->players[player-1].GetPilots()[ship-1].GetName();
      std::string un = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GetName();
      for(int i=2; i<=cmd.size(); i++) {
	switch(cmd[i]) {
	case 'a':
	  this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GS().Disable();
	  printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Inactive\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
	  break;
	case 'A':
	  this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GS().Enable();
	  printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Active\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
	  break;
	case 'f':
	case 'F':
	  this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].Flip();
	  // upgrade values changed, so refresh our local copy
	  un = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GetName();
	  printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Flipped\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
	  break;
	case 'c':
	  this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GS().ChargeDn();
	  printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Charge Down\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
	  break;
	case 'C':
	  this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GS().ChargeUp();
	  printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Charge Up\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
	  break;
	  /*
	case '(':
	  std::string newUpg;
	  i++;
	  while(cmd[i] != ')') {
	    newUpg += cmd[i++];
	  }
	  if(newUpg == "") {
	    Upgrade oldUp = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1];
	    printf("%sPossible replacements:\n", CLR_GAM);
	    for(Upgrade u : Upgrade::GetAllUpgrades()) {
	      if(u.GetType().GetType() == oldUp.GetType().GetType()) {
		printf("%s  %s\n", CLR_UPG, u.GetXws().c_str());
	      }
	    }
	  } else {
	    Upgrade oldUp = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1];
	    try{
	      this->players[player-1].GetPilots()[ship-1].ReplaceUpgrade(upg-1, oldUp.GetType().GetXws(), newUpg);
	    }
	    catch(...) {
	      printf("%s'%s' is invalid\n", CLR_ERR, newUpg.c_str());
	      return false;
	    }
	  }
	  break;
	  */
	}
      }
    }

    // end phase
    else if(std::regex_match(cmd, RX_EP)) {
      for(int player : { 0, 1}) {
        for(Pilot &p : this->players[player].GetPilots()) {
          if(p.GetModCharge().GetRecurring()) { p.GS().ChargeUp(); }
          if(p.GetModForce().GetRecurring()) { p.GS().ForceUp(); }
          for(Upgrade &u : p.GetAppliedUpgrades()) {
            if(u.GetCharge().GetRecurring()) { u.GS().ChargeUp(); }
            if(u.GetForce().GetRecurring()) { u.GS().ForceUp(); }
          }
        }
      }
    }

    else if(cmd == "q") {
      printf("%sEnter 'qqq' to quit\n", CLR_GAM);
    }

    // invalid command
    else {
      printf("%sInvalid command '%s' - enter '?' for help\n", CLR_ERR, cmd.c_str());
      return false;
    }

  }

  return true;
}
