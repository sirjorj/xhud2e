#include "imagegen_gd.h"
#include <sstream>
#include <string.h>

#if 0
#define DEBUG(f_, ...) printf((f_), __VA_ARGS__)
#else
#define DEBUG(f_, ...)
#endif

namespace ImageGenerator {

std::string ImageGen_GD::GetInfo() {
  std::stringstream ss;
  ss << "libgd " << gdVersionString();
  return ss.str();
}

ImageGen_GD::ImageGen_GD(int w, int h) : ImageGen(w,h) {
  DEBUG("ImageGen_GD()\n");
  this->gdImg = gdImageCreateTrueColor(this->width, this->height);
}

ImageGen_GD::~ImageGen_GD() {
  DEBUG("~ImageGen_GD()\n");
  gdImageDestroy(this->gdImg);
}

void ImageGen_GD::Init() {
  DEBUG("ImageGen_GD::Init()\n");
  // set transparent backgrounds
  gdImageSaveAlpha(this->gdImg, 1);
  gdImageAlphaBlending(this->gdImg, 0); // clear to enable transparent background
  this->DrawRect(Box::FromTLWH(0,0,this->width,this->height), Color::Clear()); // paint the clear background
  gdImageAlphaBlending(this->gdImg, 1); // now that background is drawn, set this again to make fonts prettier
}
  
void ImageGen_GD::DrawLine(Color c, int t, int x1, int y1, int x2, int y2) {
  DEBUG("ImageGen_GD::DrawLine()\n");
  gdImageSetThickness(this->gdImg, t);
  gdImageLine(this->gdImg, x1, y1, x2, y2, this->GetGdColor(c));

}

void ImageGen_GD::DrawRect(Box b, Color c) {
  DEBUG("ImageGen_GD::DrawRect()\n");
  gdImageFilledRectangle(this->gdImg, b.Left(), b.Top(), b.Right(), b.Bottom(), this->GetGdColor(c));
}

void ImageGen_GD::DrawText(Font f, double s, Align a, std::string t, Color c, int x, int y) {
  DEBUG("ImageGen_GD::DrawText()\n");
  /*
  // libgd renders text from a baseline and not within a box
  // calculate the actual text size to get the box top
  Box ats = this->GetTextSize(f, s, t);
  printf("  ATS T:%d L:%d W:%d H:%d\n", ats.Top(), ats.Left(), ats.Width(), ats.Height());
  // find the offset so we know where to render the text to put it in the target box 'b'
  int vOffset = 0;
  // add the inverted Top to push it down into the box
  vOffset += ats.Top() * -1;
  // add half of the height difference to vertically center it
  vOffset += (b.Height() - ats.Height()) / 2;
  */

  switch(a) {
  case Align::Left:
    // nothing to do here
    break;
  case Align::Center:
    x = x - (this->GetTextSize(f, s, t).Width() / 2);
    break;
  case Align::Right:
    x = x - this->GetTextSize(f, s, t).Width();
    break;
  }

  // now render the text  
  int brect[8];
  char *err = gdImageStringFT(this->gdImg, // img
			      &brect[0],   // rect
			      this->GetGdColor(c), // color
			      (char*)f.GetFile().c_str(), // font
			      s,                 // size
			      0.0,               // angle
			      x,//b.Left(),          // x
			      y,//b.Top()+vOffset,           // y
			      (char*)t.c_str()); // string
  if(err) { printf("%s\n", err); return; } //ImageGenerator::Box::FromTLBR(0,0,0,0); }
}

void ImageGen_GD::Finish() {
}

void ImageGen_GD::Save(std::string name) {
  bool isStdout = (strcmp(name.c_str(), "--") == 0);
  FILE *out;
  if(isStdout) {
    out = stdout;
  } else {
    out = fopen(name.c_str(), "wb");
    if(out == 0) {
      printf("error opening file (%s)\n", name.c_str());
      return;
    }
  }
  gdImagePng(this->gdImg, out);
  if(!isStdout) {
    fclose(out);
  }
  
}

// privates
Box ImageGen_GD::GetTextSize(Font f, double s, std::string t) {
  // [0,1] lower-left  X,Y
  // [2,3] lower-right X,Y
  // [4,5] upper-right X,Y
  // [6,7] upper-left  X,Y
  int brect[8];
  char *err = gdImageStringFT(0,         // img
			      &brect[0], // rect
			      0,         // color
			      (char*)f.GetFile().c_str(), // font
			      s,         // size
			      0.0,       // angle
			      0,         // x
			      0,         // y
			      (char*)t.c_str()); // string
  if(err) { DEBUG("%s\n", err); return Box::FromTLBR(0,0,0,0); }
  return Box::FromTLBR(brect[7], brect[6], brect[3], brect[2]);
}

int ImageGen_GD::GetGdColor(const Color& c) {
  DEBUG("GetGdColor()\n");
  std::string key = c.GetHexA();
  if(this->colorPalette.find(key) == this->colorPalette.end()) {
    DEBUG("  adding for key:%s\n", key.c_str());
    // invert alpha because libgd does it backwards
    this->colorPalette[key] = gdImageColorAllocateAlpha(this->gdImg, c.GetRed(), c.GetGreen(), c.GetBlue(), ~c.GetAlpha());
  }
  DEBUG("  [%s]->%d\n", key.c_str(), this->colorPalette[key]);
  return this->colorPalette[key];
}


}
