#include "printhelpers.h"
#include "colors.h"
#include <libxwing2/converter.h>
#include <algorithm>

std::string GetDifficultyColor(Dif d) {
  switch(d) {
  case Dif::Blue:  return CLR_MDB;
  case Dif::White: return CLR_MDW;
  case Dif::Red:   return CLR_MDR;
  default:         return CLR_TXT;
  }
}

std::string GetArc(Arc a) {
  switch(a) {
  case Arc::Front:        return "F";
  case Arc::Rear:         return "R";
  case Arc::Bullseye:     return "B";
  case Arc::FullFront:    return "FF";
  case Arc::FullRear:     return "FR";
  case Arc::SingleTurret: return "ST";
  case Arc::DoubleTurret: return "DT";
  default:                return "?";
  }
}



void PrintManeuverChart(Maneuvers maneuvers) {
  int8_t min = 10;
  int8_t max = -10;
  for(auto a : maneuvers) { if(a.speed > max) {max = a.speed;} if(a.speed < min) {min=a.speed;} }

  Maneuver m;
  for(int i=max; i>=min; i--) {
    printf(" %s%1d|", CLR_TXT, i);

    // standard maneuvers
    for(Brn b : {Brn::LTurn, Brn::LBank, (i==0) ? Brn::Stationary : Brn::Straight, Brn::RBank, Brn::RTurn}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
      } else {
        printf("  ");
      }
    }

    // special maneuvers
    for(Brn b : {Brn::KTurn, Brn::LSloop, Brn::RSloop, Brn::LTroll, Brn::RTroll}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
      }
    }

    printf("\n");
  }
  // reverse maneuvers
  for(int i=min; i<=max; i++) {
    bool doIt = false;
    for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
      if(FindManeuver(maneuvers, i, b, m)) {
        doIt = true;
        break;
      }
    }
    if(doIt) {
      printf("%s-%1d|  ", CLR_TXT, i);
      for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
        if(FindManeuver(maneuvers, i, b, m)) {
          printf(" %s%s%s", GetDifficultyColor(m.difficulty).c_str(), Bearing::GetBearing(m.bearing).GetCharacter().c_str(), NORMAL);
        } else {
          printf("  ");
        }
      }
    }
  }
}



void PrintShips(std::vector<Ship> ships) {
  std::vector<std::string> done;
  int nLen=0, xLen=0;
  for(auto s : ships) {
    nLen = std::max(nLen, (int)s.GetName().size());
    xLen = std::max(xLen, (int)converter::xws::GetShip(s.GetShp()).size());
  }

  for(auto s : ships) {
    bool isReleased = false;
    for(Release r : Release::GetAllReleases()) {
      if(!r.IsUnreleased()) {
        for(RelShip rs : r.GetShips()) {
          if(rs.ship == s.GetShp()) {
            isReleased = true;
            break;
          }
        }
        if(isReleased) { break; }
      }
    }
    printf("%s%-*s %-*s %s%s\n", CLR_SHP,
	   nLen, s.GetName().c_str(),
	   xLen, converter::xws::GetShip(s.GetShp()).c_str(),
	   CLR_UNR, isReleased ? "" : "[UNRELEASED]");
  }
}

int getDigits(int n) {
  int ret = 0;
  while(n != 0) {
    ret++;
    n /= 10;
  }
  return ret;
}

void PrintShip(Ship ship) {
  std::vector<Pilot> pilots;
  std::string name = "";
  int nameLen=0, facLen=0, limLen=0, costLen=0, fcLen=0;
  Maneuvers maneuvers;
  for(Pilot p : Pilot::GetAllPilots()) {
    if(p.GetShp() == ship.GetShp()) {
      if(name=="") { name = p.GetShip().GetName(); }
      nameLen = std::max(nameLen, (int)p.GetName().size());
      facLen  = std::max(facLen,  (int)p.GetFaction().GetShort().size());
      limLen  = std::max(limLen,  (int)p.GetLimited());
      costLen = std::max(costLen, (int)(p.GetNatCost() ? getDigits(*p.GetNatCost()) : 3));
      fcLen   = std::max(fcLen,   (int)((p.GetNatForce().GetCapacity()>0) ? 1 : 0) + (p.GetNatForce().GetRecurring() ? 1 : 0));
      fcLen   = std::max(fcLen,   (int)((p.GetNatCharge().GetCapacity()>0) ? 1 : 0) + (p.GetNatCharge().GetRecurring() ? 1 : 0));
      if(maneuvers.size() == 0) { maneuvers = p.GetNatManeuvers(); }
      pilots.push_back(p);
    }
  }

  std::sort(pilots.begin(), pilots.end(), [] (const Pilot& a, const Pilot& b) {
      return (a.GetFac() < b.GetFac())
	|| ((a.GetFac() == b.GetFac()) && (a.GetNatInitiative() > b.GetNatInitiative()));
    });

  printf("%s%s\n", CLR_SHP, name.c_str());

  // print the maneuver chart
  printf("\n");
  PrintManeuverChart(maneuvers);
  printf("\n");

  // see if any of the ships have Talent
  bool shipHasEpt = false;
  for(auto p : pilots) {
    std::optional<UpgradeBar> ub = p.GetNatUpgradeBar();
    if(ub) {
      for(const UpgradeSlot& s : *ub) {
	if(s.CanEquip(UpT::Talent)) { shipHasEpt = true; break; }
      }
      if(shipHasEpt) break;
    }
  }

  // print the info
  for(auto p : pilots) {
    // faction
    printf("%s%-*s ", CLR_TXT, facLen, p.GetFaction().GetName().c_str());
    // cost
    printf("%s%*s ", CLR_CST, costLen, p.GetNatCost() ? std::to_string(*p.GetNatCost()).c_str() : "???");
    // limited
    printf("%s", CLR_UNI);
    for(int l=0; l<(limLen-p.GetLimited()); l++) { printf(" "); }
    for(int l=0; l<p.GetLimited();          l++) { printf("*"); }
    // name
    printf("%s%-*s", CLR_PLT, nameLen+limLen, p.GetName().c_str());
    // initiative
    printf("%s%d ", CLR_INI, p.GetNatInitiative());
    // attack(s)
    for(PriAttack a : p.GetNatAttacks()) {
      printf("%s%s%d ", CLR_ATK, GetArc(a.arc).c_str(), a.value);
    }
    // agility
    printf("%s%d ", CLR_AGI, p.GetNatAgility());
    // hull
    printf("%s%d ", CLR_HUL, p.GetNatHull());
    // shield
    if(p.GetNatShield().GetCapacity()) {
      printf("%s%d ", CLR_SHD, p.GetNatShield().GetCapacity());
    }
    if(fcLen) {
      // charge
      if(p.GetNatCharge().GetCapacity()) {
	printf("%s%d%s ", CLR_CHG, p.GetNatCharge().GetCapacity(), p.GetNatCharge().GetRecurring() ? "^" : " ");
      }
      // force
      else if(p.GetNatForce().GetCapacity()) {
	printf("%s%d%s ", CLR_FRC, p.GetNatForce().GetCapacity(), p.GetNatForce().GetRecurring() ? "^" : " ");
      }
      else {
	printf("%*s", fcLen+1, "");
      }
    }
    // actions
    for(std::list<SAct> al : p.GetNatActions()) {
      printf("%s[", CLR_TXT);
      bool first=true;
      for(SAct a : al) {
	if(first) { first = false; }
	else      { printf("%s%s", CLR_TXT, ">"); }
	printf("%s%s", GetDifficultyColor(a.difficulty).c_str(), Action::GetAction(a.action).GetShortName().c_str());
      }
      printf("%s]", CLR_TXT);
    }
    printf(" ");
    // upgrades
    bool hasTF = false;
    std::optional<UpgradeBar> ub = p.GetNatUpgradeBar();
    if(ub) {
      for(const UpgradeSlot& s : *ub) {
	if(s.CanEquip(UpT::Talent) || s.CanEquip(UpT::Force)) {
	  hasTF = true;
	  break;
	}
      }
    }
    if(!hasTF) {
      printf("     ");
    }
    if(ub) {
      for(const UpgradeSlot& s : *ub) {
	printf("%s[", CLR_TXT);
	bool first = true;
	for(UpT t : s.GetTypes()) {
	  if(first) { first = false; }
	  else      { printf("%s,", CLR_TXT); }
	  printf("%s%s", CLR_UPG, UpgradeType::GetUpgradeType(t).GetShortName().c_str());
	}
	printf("%s]", CLR_TXT);
      }
    }
    // ability/text
    printf(" - ");
    printf("%s%s", p.HasAbility() ? CLR_TXT : CLR_LBL, p.GetText().GetCleanText().c_str());
    printf("\n");
  }
}



void PrintPilots(std::vector<Pilot> pilots) {
  bool hasUnreleased=false;
  int limLen=0, nameLen=0, facLen=0, shipLen=0, xwsLen=0;
  for(auto p : Pilot::GetAllPilots()) {
    hasUnreleased |= p.IsUnreleased();
    limLen  = std::max(limLen,  (int)p.GetLimited());
    nameLen = std::max(nameLen, (int)p.GetName().size());
    facLen  = std::max(facLen,  (int)p.GetFaction().GetName().size());
    shipLen = std::max(shipLen, (int)p.GetShip().GetName().size());
    xwsLen  = std::max(xwsLen,  (int)converter::xws::GetPilot(p.GetPlt()).size());
  }
  for(auto p : pilots) {
    std::string facCol;
    switch(p.GetFac()) {
    case Fac::Rebel:      facCol=CLR_REB; break;
    case Fac::Imperial:   facCol=CLR_IMP; break;
    case Fac::Scum:       facCol=CLR_SCM; break;
    case Fac::Resistance: facCol=CLR_RES; break;
    case Fac::FirstOrder: facCol=CLR_FO;  break;
    case Fac::Republic:   facCol=CLR_REP; break;
    case Fac::Separatist: facCol=CLR_SEP; break;
    default:              facCol="";      break;
    }
    // unreleased
    if(hasUnreleased) {
      printf("%s%s ", p.IsUnreleased() ? CLR_UNR : CLR_LBL, p.IsUnreleased() ? "U" : " ");
    }
    // limited
    printf("%s", CLR_UNI);
    for(int l=0; l<(limLen-p.GetLimited()); l++) { printf(" "); }
    for(int l=0; l<p.GetLimited();          l++) { printf("*"); }
    // pilot
    printf("%s%-*s", CLR_PLT, nameLen, p.GetName().c_str());
    // faction
    printf(" %s%-*s", facCol.c_str(), facLen, p.GetFaction().GetName().c_str());
    // ship
    printf(" %s%-*s", CLR_SHP, shipLen, p.GetShip().GetName().c_str());
    // xws
    printf(" %s%-*s", CLR_PLT, xwsLen, converter::xws::GetPilot(p.GetPlt()).c_str());
    printf("\n");        
  }
}

std::string GetLimitedString(uint8_t n) {
  std::string ret;
  for(int i=0; i<n; i++) { ret += "*"; }
  return ret;
}

void PrintPilot(Pilot p) {
  printf("%sPilot:      %s%s%s%s", CLR_LBL, CLR_UNI, GetLimitedString(p.GetLimited()).c_str(), CLR_PLT, p.GetName().c_str());
  if(p.IsUnreleased()) { printf(" %sUNRELEASED", CLR_UNR); }
  printf("\n");
  printf("%sSubtitle:   %s%s\n", CLR_LBL, CLR_TXT, p.GetSubtitle().c_str());
  printf("%sFaction:    %s%s\n", CLR_LBL, CLR_TXT, p.GetFaction().GetName().c_str());
  printf("%sShip:       %s%s\n", CLR_LBL, CLR_SHP, p.GetShip().GetName().c_str());
  printf("%sCost:       %s%s\n", CLR_LBL, CLR_CST, p.GetNatCost() ? std::to_string(*p.GetNatCost()).c_str() : "???");
  printf("%sInitiative: %s%d\n", CLR_LBL, CLR_INI, p.GetNatInitiative());
  printf("%sStats:      ", CLR_LBL);
  for(PriAttack a : p.GetNatAttacks()) {
    printf("%s%s%d ", CLR_ATK, GetArc(a.arc).c_str(), a.value);
  }
  printf("%s%d ", CLR_AGI, p.GetNatAgility());
  printf("%s%d ", CLR_HUL, p.GetNatHull());
  if(p.GetNatShield().GetCapacity()) {
    printf("%s%d ", CLR_SHD, p.GetNatShield().GetCapacity());
  }
  if(p.GetNatCharge().GetCapacity()) {
    printf("%s%d%s ", CLR_CHG, p.GetNatCharge().GetCapacity(), p.GetNatCharge().GetRecurring() ? "^" : " ");
  }
  else if(p.GetNatForce().GetCapacity()) {
    printf("%s%d%s ", CLR_FRC, p.GetNatForce().GetCapacity(), p.GetNatForce().GetRecurring() ? "^" : " ");
  }
  printf("\n");
  // actions
  printf("%sActions:    ", CLR_LBL);
  for(std::list<SAct> al : p.GetNatActions()) {
    printf("%s[", CLR_TXT);
    bool first=true;
    for(SAct a : al) {
      if(first) { first = false; }
      else      { printf("%s%s", CLR_TXT, ">"); }
      printf("%s%s", GetDifficultyColor(a.difficulty).c_str(), Action::GetAction(a.action).GetShortName().c_str());
    }
    printf("%s]", CLR_TXT);
  }
  printf("\n");
  // upgrades
  printf("%sUpgrades:   ", CLR_LBL);
  bool hasTF = false;
  std::optional<UpgradeBar> ub = p.GetNatUpgradeBar();
  if(ub) {
    for(const UpgradeSlot& s : *ub) {
      if(s.CanEquip(UpT::Talent) || s.CanEquip(UpT::Force)) {
	hasTF = true;
	break;
      }
    }
  }
  if(!hasTF) {
    printf("     ");
  }
  if(ub) {
    for(const UpgradeSlot &s : *ub) {
      printf("%s[", CLR_TXT);
      bool first = true;
      for(UpT t : s.GetTypes()) {
	if(first) { first = false; }
	else      { printf("%s,", CLR_TXT); }
	printf("%s%s", CLR_UPG, UpgradeType::GetUpgradeType(t).GetShortName().c_str());
      }
      printf("%s]", CLR_TXT);
    }
    printf("\n");
  }
  if(p.HasAbility()) {
    printf("%sAbility:    %s%s\n", CLR_LBL, CLR_TXT, p.GetText().GetCleanText().c_str());
  } else {
    printf("%sText:       %s%s\n", CLR_LBL, CLR_TXT, p.GetText().GetCleanText().c_str());
  }
  std::vector<Release> rs = Release::GetByPilot(p.GetPlt());
  bool isFirst = true;
  printf("%sAvailable:  %s", CLR_LBL, CLR_TXT);
  for(Release r : rs) {
    if(isFirst) { isFirst = false; }
    else        { printf(", "); }
    printf("%s", r.GetName().c_str());
  }
  printf("\n");
  printf("%sManeuvers:\n", CLR_LBL);
  PrintManeuverChart(p.GetNatManeuvers());
}



void PrintUpgrades(std::vector<Upgrade> upgrades) {
  bool hasUnreleased=false;
  int limLen=0, nameLen=0, typeLen=0, xwsLen=0;
  for(auto u : Upgrade::GetAllUpgrades()) {
    hasUnreleased |= u.IsUnreleased();
    limLen  = std::max(limLen,  (int)u.GetLimited());
    nameLen = std::max(nameLen, (int)u.GetName().size());
    typeLen = std::max(typeLen, (int)u.GetUpgradeType().GetName().size());
    xwsLen  = std::max(xwsLen,  (int)converter::xws::GetUpgrade(u.GetUpg()).size());
  }
  for(auto u : upgrades) {
    // unreleased
    if(hasUnreleased) {
      printf("%s%s ", u.IsUnreleased() ? CLR_UNR : CLR_LBL, u.IsUnreleased() ? "U" : " ");
    }
    // limited
    printf("%s", CLR_UNI);
    for(int l=0; l<(limLen-u.GetLimited()); l++) { printf(" "); }
    for(int l=0; l<u.GetLimited();          l++) { printf("*"); }
    // name
    printf("%s%-*s", CLR_UPG, nameLen, u.GetName().c_str());
    // type
    printf("  %s%-*s", CLR_UPT, typeLen, u.GetUpgradeType().GetName().c_str());
    // xws
    printf("  %s%-*s", CLR_UPG, xwsLen, converter::xws::GetUpgrade(u.GetUpg()).c_str());
    printf("\n");
  }
}



void PrintUpgrade(Upgrade u) {
  int sides = u.IsDualSided() ? 2 : 1;
  bool ds = u.IsDualSided();
  if(ds) {
    printf("%sName:    %s%s%s%s\n", CLR_LBL, CLR_UNI, GetLimitedString(u.GetLimited()).c_str(), CLR_UPG, u.GetName().c_str());
  }
  for(int i=1; i<=sides; i++) {
    if(ds) {
      printf("%sSide %d:\n", CLR_TXT, i);
      printf("%s%sTitle:    %s%s%s%s\n", CLR_LBL, ds ? " " : "", CLR_UNI, GetLimitedString(u.GetLimited()).c_str(), CLR_UPG, u.GetTitle().c_str());
    } else {
      printf("%s%sName:    %s%s%s%s\n", CLR_LBL, ds ? " " : "", CLR_UNI, GetLimitedString(u.GetLimited()).c_str(), CLR_UPG, u.GetTitle().c_str());
    }
    printf("%s%sType:    %s%s\n", CLR_LBL, ds ? " " : "", CLR_UPT, u.GetUpgradeType().GetName().c_str());
    if(u.GetAttackStats()) {
      std::string range;
      if(u.GetAttackStats()->minRange == u.GetAttackStats()->maxRange) {
	range = std::to_string(u.GetAttackStats()->minRange);
      } else {
	range = std::to_string(u.GetAttackStats()->minRange) + "-" + std::to_string(u.GetAttackStats()->maxRange);
      }
      printf("%s%sAttack:  %s%hhu%s (%s%s%s) %s\n",
	     CLR_LBL, u.IsDualSided() ? " " : "",
	     CLR_ATK, u.GetAttackStats()->value,
	     CLR_TXT, CLR_RNG, range.c_str(), CLR_TXT,
	     u.GetAttackStats()->ordnance ? "[ORDNANCE]" : "");
    }
    printf("%s%sText:    %s%s\n", CLR_LBL, u.IsDualSided() ? " " : "", CLR_TXT, u.GetText().GetCleanText().c_str());
    if(u.IsDualSided()) {
      u.Flip();
    }
  }
  printf("%sCost:    %s", CLR_LBL, CLR_CST);//%s%hhd\n", CLR_LBL, CLR_CST, u.GetCost());
  std::optional<Cost> c = u.GetCost();
  if(!c) {
    printf("???");
  } else {
    std::vector<int16_t> cs = c->GetCosts();
    switch(u.GetCost()->GetCsT()) {
    case CsT::Const: printf("%d\n", cs[0]); break;
    case CsT::Base:  printf("S:%d / M:%d / L:%d\n", cs[0], cs[1], cs[2]); break;
    case CsT::Agi:   printf("A0:%d / A1:%d / A2:%d / A3:%d\n", cs[0], cs[1], cs[2], cs[3]); break;
    case CsT::Init:  printf("i0:%d / i1:%d / i2:%d / i3:%d / i4:%d / i5:%d / i6:%d\n", cs[0], cs[1], cs[2], cs[3], cs[4], cs[5], cs[6]); break;
    }
  }
  printf("%sAvailable:%s", CLR_LBL, CLR_TXT);
  std::vector<Release> rs = Release::GetByUpgrade(u.GetUpg());
  bool isFirst = true;
  for(Release r : rs) {
    if(!isFirst) {
      printf(",");
    } else {
      isFirst = false;
    }
    printf(" %s", r.GetName().c_str());
  }
  printf("\n");
}



void PrintRelease() {
  printf("%sSKU   Name\n", CLR_TXT);
  int relLen=0;
  for(auto r : Release::GetAllReleases()) {
    if(relLen < r.GetName().length()) relLen = r.GetName().length();
  }

  for(auto r : Release::GetAllReleases()) {
    printf("%s%s %s%-*s%s%s\n", CLR_LBL, r.GetSku().c_str(), CLR_TXT, relLen, r.GetName().c_str(), CLR_UNR, r.IsUnreleased() ? " [UNRELEASED]" : "");
  }
}

void PrintRelease(std::string sku) {
  bool first = true;
  Release r = Release::GetRelease(sku);
  printf("%sName:  %s%s%s%s\n", CLR_LBL, CLR_TXT, r.GetName().c_str(), CLR_UNR, r.IsUnreleased() ? " [UNRELEASED]" : "");
  printf("%sSKU:   %s%s\n",     CLR_LBL, CLR_TXT, r.GetSku().c_str());
  printf("%sISBN:  %s%s\n",     CLR_LBL, CLR_TXT, r.GetIsbn().c_str());
  printf("%sAnnounced: %s%04d.%02d.%02d\n", CLR_LBL, CLR_TXT, r.GetAnnounceDate().year, r.GetAnnounceDate().month, r.GetAnnounceDate().date);
  printf("%sReleased:  %s%04d.%02d.%02d\n", CLR_LBL, CLR_TXT, r.GetReleaseDate().year, r.GetReleaseDate().month, r.GetReleaseDate().date);
  printf("\n");

  // ships
  first=true;
  for(RelShip s : r.GetShips()) {
    if(first) {
      printf("%sShip(s): ", CLR_LBL);
      first = false;
    } else {
      printf("         ");
    }
    printf("%s%s", CLR_SHP, Ship::GetShip(s.ship).GetName().c_str());
    if(s.desc != "") {
      printf(" (%s)", s.desc.c_str());
    }
    printf("\n");
  }

  printf("\n");

  // pilots
  first=true;
  for(Plt plt : r.GetPilots()) {
    if(first) {
      printf("%sPilot(s): ", CLR_LBL);
      first = false;
    } else {
      printf("          ");
    }
    Pilot p = Pilot::GetPilot(plt);
    printf("%s%s %s- %s%s%s%s\n", CLR_SHP, p.GetShip().GetName().c_str(), CLR_TXT, CLR_UNI, GetLimitedString(p.GetLimited()).c_str(), CLR_PLT, p.GetName().c_str());
  }
  if(!first) { printf("\n"); }

  // upgrades
  first=true;
  for(Upg upg : r.GetUpgrades()) {
    if(first) {
      printf("%sUpgrade(s): ", CLR_LBL);
      first = false;
    } else {
      printf("            ");
    }
    Upgrade u = Upgrade::GetUpgrade(upg);
    printf("%s%s %s- %s%s%s%s\n", CLR_UPT, u.GetUpgradeType().GetName().c_str(), CLR_TXT, CLR_UNI, GetLimitedString(u.GetLimited()).c_str(), CLR_UPG, u.GetName().c_str());
  }
  if(!first) { printf("\n"); }

  // conditions
  first=true;
  for(Cnd c : r.GetConditions()) {
    if(first) {
      printf("%sCondition(s): ", CLR_LBL);
      first = false;
    } else {
      printf("              ");
    }
    printf("%s%s\n", CLR_TXT, Condition::GetCondition(c).GetName().c_str());
  }
  if(!first) { printf("\n"); }

  // tokens
  first=true;
  std::map<Tok,std::shared_ptr<Tokens>> tc = r.GetTokens().GetAllTokens();
  for(const std::pair<Tok,std::shared_ptr<Tokens>> t : tc) {
    Tok tok = t.first;
    std::shared_ptr<Tokens> tokensSP = t.second;
    Tokens tokens = *tokensSP.get();
    int count = tokens.GetCount();
    if(first) {
      printf("%sToken(s): ", CLR_LBL);
      first = false;
    } else {
      printf("          ");
    }
    printf("%s%sx%d %s", CLR_TXT, count>9 ? "" : " ", count, Token::GetToken(tok).GetName().c_str());
    if(tok == Tok::Ship) {
      printf(" %s(", CLR_LBL);
      bool f = true;
      ShTokens sht = *dynamic_cast<ShTokens*>(tokensSP.get());
      std::vector<std::pair<Plt,Plt>> shTokens = sht.GetPilots();
      for(std::pair<Plt,Plt> shToken : shTokens) {
        if(!f) { printf(", "); } else { f = false; }
        printf("%s/%s", Pilot::GetPilot(shToken.first).GetShortName().c_str(), Pilot::GetPilot(shToken.second).GetShortName().c_str());
      }
      printf(")");
    }
    else if((tok==Tok::ID) || (tok==Tok::Lock)) {
      printf(" %s(", CLR_LBL);
      bool f = true;
      IDTokens idt = *dynamic_cast<IDTokens*>(tokensSP.get());
      std::map<int,int> idCounts = idt.GetIDCounts();
      for(std::pair<int,int> idCount : idCounts) {
        if(!f) { printf(", "); } else { f = false; }
        printf("%d:x%d", idCount.first, idCount.second);
      }
      printf(")");
    }
    printf("\n");
  }
  if(!first) { printf("\n"); }

  // urls
  if(r.GetAnnouncementArticle().url != "") {
    printf("%sAnnouncement: %s%s\n", CLR_LBL, CLR_TXT, r.GetAnnouncementArticle().url.c_str());
  }
  if(r.GetPreviewArticles().size() > 0) {
    first=true;
    for(const Article& a : r.GetPreviewArticles()) {
      if(first) {
        printf("%sPreview:      ", CLR_LBL);
        first = false;
      } else {
        printf("              ");
      }
      printf("%s%s\n", CLR_TXT, a.url.c_str());
    }
  }
  if(r.GetReleaseArticle().url != "") {
    printf("%sRelease:      %s%s\n", CLR_LBL, CLR_TXT, r.GetReleaseArticle().url.c_str());
  }
}



void PrintSquad(Squad s) {
  printf("%s\"%s\" [%s]\n", CLR_TXT, s.GetName().c_str(), Faction::GetFaction(s.GetFac()).GetName().c_str());
  if(!s.GetDescription().empty()) {
    printf("%s\n", s.GetDescription().c_str());
  }
  printf("%s%s\n", CLR_TXT, converter::yasb::GetSquad(s).c_str());
  printf("\n");

  for(auto p : s.GetPilots()) {

    printf("%s[%s] %s%s%s%s%s - %s%s\n",
           CLR_CST,p.GetModCost() ? std::to_string(*p.GetModCost()).c_str() : "???", CLR_UNI, GetLimitedString(p.GetLimited()).c_str(), CLR_PLT, p.GetName().c_str(), CLR_TXT, CLR_SHP, p.GetShip().GetName().c_str());

    // skill
    printf("%s%hhu", CLR_INI, p.GetNatInitiative());
    if(p.GetNatInitiative() != p.GetModInitiative()) {
      printf("(%d)", p.GetModInitiative());
    }
    printf("%s - ", CLR_TXT);

    // attack
    bool first = true;
    for(PriAttack a : p.GetNatAttacks()) {
      if(first) { first = false; }
      else      { printf("%s/", CLR_TXT); }
      printf("%s%s%d", CLR_ATK, GetArc(a.arc).c_str(), a.value);
    }
    printf("%s/", CLR_TXT);

    // agility
    printf("%s%hhu", CLR_AGI, p.GetNatAgility());
    if(p.GetNatAgility() != p.GetModAgility()) {
      printf("(%hhu)", p.GetModAgility());
    }
    printf("%s/", CLR_TXT);

    // hull
    printf("%s%hhu", CLR_HUL, p.GetNatHull());
    if(p.GetNatHull() != p.GetModHull()) {
      printf("(%hhu)", p.GetModHull());
    }
    printf("%s/", CLR_TXT);

    // shield
    printf("%s%hhu", CLR_SHD, p.GetNatShield().GetCapacity());
    if(p.GetNatShield().GetCapacity() != p.GetModShield().GetCapacity()) {
      printf("(%hhu)", p.GetModShield().GetCapacity());
    }

    // charge
    if(p.GetNatCharge().GetCapacity()) {
      printf("%s/", CLR_TXT);
      printf("%s%hhu", CLR_CHG, p.GetNatCharge().GetCapacity());
      if(p.GetNatCharge().GetCapacity() != p.GetModCharge().GetCapacity()) {
	printf("(%hhu)", p.GetModCharge().GetCapacity());
      }
      if(p.GetNatCharge().GetRecurring()) {
	printf("^");
      }
    }

    // force
    if(p.GetNatForce().GetCapacity()) {
      printf("%s/", CLR_TXT);
      printf("%s%hhu", CLR_FRC, p.GetNatForce().GetCapacity());
      if(p.GetNatForce().GetCapacity() != p.GetModForce().GetCapacity()) {
	printf("(%hhu)", p.GetModForce().GetCapacity());
      }
      if(p.GetNatForce().GetRecurring()) {
	printf("^");
      }
    }
    printf("%s - ", CLR_TXT);

    // actions
    for(std::list<SAct> al : p.GetNatActions()) {
      printf("%s[", CLR_TXT);
      bool first=true;
      for(SAct a : al) {
	if(first) { first = false; }
	else      { printf("%s%s", CLR_TXT, ">"); }
	printf("%s%s", GetDifficultyColor(a.difficulty).c_str(), Action::GetAction(a.action).GetShortName().c_str());
      }
      printf("%s]", CLR_TXT);
    }

    // upgrades
    printf(" - ");
    std::optional<UpgradeBar> ub = p.GetNatUpgradeBar();
    if(ub) {
      for(const UpgradeSlot& s : *ub) {
	printf("%s[", CLR_TXT);
	bool first = true;
	for(UpT t : s.GetTypes()) {
	  if(first) { first = false; }
	  else      { printf("%s,", CLR_TXT); }
	  printf("%s%s", CLR_UPG, UpgradeType::GetUpgradeType(t).GetShortName().c_str());
	}
	printf("%s]", CLR_TXT);
      }
    }
    printf("\nUpgrades:");
    for(auto u : p.GetAppliedUpgrades()) {
      printf(" [%s%s%s]", CLR_UPG, u.GetName().c_str(), CLR_TXT);
    }
    printf("\n");

    // ship ability
    if(p.GetShipAbility()) {
      printf("%s%s: %s\n", CLR_TXT, p.GetShipAbility()->GetName().c_str(), p.GetShipAbility()->GetText().GetCleanText().c_str());
    }

    // ability
    if(p.HasAbility()) {
      printf("%s%s\n", CLR_TXT, p.GetText().GetCleanText().c_str());
    }

    // draw the maneuver chart
    Maneuvers ms = p.GetModManeuvers();

    PrintManeuverChart(ms);
    printf("\n");
  }
}
