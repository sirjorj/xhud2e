#include "colors.h"
#include "game.h"
#include "imagegen.h"
#include "portable.h"
#include "printhelpers.h"
#include "settings.h"
#include <libxwing2/converter.h>
#include <libxwing2/release.h>
#include <iostream>
#include <string>
#include <string.h>
#include <sys/stat.h>

const static std::string VERSTR = "0.4";

using namespace libxwing2;

std::string GetVersionInfo(const SettingsManager& s) {
  return "xhud v" + VERSTR + " for Second Edition (using " +  GetImageGenInfo(s.GetSetting("run.outformat")) + ")";
}

static void printOptions(const SettingsManager& s) {
  printf("%s\n", GetVersionInfo(s.GetSetting("run.outformat")).c_str());
  printf("Usage: './xhud *option* {params}'\n");
  printf("Options:\n");
  printf("  check             - check for required files\n");
  printf("  settings          - read and display the settings file\n");
  printf("  ship              - prints available ships\n");
  printf("  ship {S}          - prints details on ship {s}\n");
  printf("  pilot             - prints available pilots\n");
  printf("  pilot {P F S}     - prints details on pilot {P F S} (Pilot, Faction, Ship)\n");
  printf("  upgrade           - prints available upgrades\n");
  printf("  upgrade {T U}     - prints details on upgrade {T U} (Type, Upgrade)\n");
  printf("  release           - prints all releases\n");
  printf("  release {SKU}     - prints details on release {SKU}\n");
  printf("                      NOTE: {S}, {P}, {T}, and {U} are the xws keys\n");
  printf("  squad {Q}         - dump the squad {Q} to terminal ({Q} is a .xws file)\n");
  printf("  validate {Q}      - validate the squad {Q}\n");
  printf("  img {Q} {I}       - generate image (I) for the squad {Q}\n");
  printf("  run {Q1} {Q2} {P} - run a game with the 2 specified squads, outputting image(s) in directory at path (P)\n");
  //printf("  build {f}         - start the squad builder\n");
}



static bool fileExists(const std::string file) {
  struct stat buffer;
  return (stat (file.c_str(), &buffer) == 0);
}



static std::vector<std::pair<std::string,bool>> CheckFonts() {
  std::vector<std::string> fontFiles = {
    { "xwing-miniatures-ships.ttf"  },
    { "xwing-miniatures.ttf"        },
    { "SquarishSansCTRegularSC.ttf" },
    { "xwstats.ttf"                 }
  };
  std::vector<std::pair<std::string,bool>> ret;
  for(auto ff : fontFiles) {
    std::string filename = "./fonts/" + ff;
    bool hasIt = false;
    if(fileExists(filename)) {
      hasIt = true;
    }
    ret.push_back({ff, hasIt});
  }
  return ret;
}



bool ValidateList(std::string listFile) {
  //printf("Verifying %-36s - ", listFile.c_str());
  fflush(stdout);
  try {
    Squad sq = Squad(converter::xws::GetSquad(listFile));
    std::vector<std::string> issues = sq.Validate();
    if(issues.size() == 0) {
      printf("Ok\n");
    } else {
      printf("INVALID\n");
      for(std::string s : issues) {
	printf("  \e[1;31m%s\x1B[0m\n", s.c_str());
      }
    }
    if(issues.size() > 0) {
      return false;
    } else {
      return true;
    }
  }
  catch(std::invalid_argument e) {
    printf("EXCEPTION\n");
    printf("  \e[1;31m%s\x1B[0m\n", e.what());
    return false;
  }
}



std::string GetConfigFile() {
  std::string configPath;
  char *home = std::getenv("HOME");
  if(home) {
    configPath += home;
  }
  configPath += "/.xhud.conf";
  return configPath;
}



Squad MakeSquad(std::string file) {
  if(file.substr(0,26) == "https://raithos.github.io/") {
    return Squad(converter::yasb::GetSquad(file));
  }
  else if(file == "--") {
    return Squad(converter::xws::GetSquad(std::cin));
  } else {
    return Squad(converter::xws::GetSquad(file));
  }
}



int main(int argc, char* argv[]) {

  if(pledge("stdio rpath wpath cpath", NULL) == -1) err(1, "pledge");

  SettingsManager settings = SettingsManager(GetConfigFile());

  if(argc == 1) {
    printOptions(settings);
  }

  else if(strcmp(argv[1], "settings") == 0) {
    settings.VerifySettings(0);
    settings.Dump();
  }

  else if(strcmp(argv[1], "check") == 0) {
    printf("Checking for required fonts...\n");
    std::vector<std::pair<std::string,bool>> cf = CheckFonts();
    int fontlen=0;
    for(auto f : cf) { if(f.first.length() > fontlen) fontlen = f.first.length(); };
    for(auto f : cf) { printf("  %-*s - %s\n", fontlen, f.first.c_str(), f.second ? "Ok" : "NOT FOUND"); }
    printf("\n");
    Ship::SanityCheck();
    printf("\n");
    Pilot::SanityCheck();
    printf("\n");
    Upgrade::SanityCheck();
    printf("\n");
  }

  else if((strcmp(argv[1], "ship") == 0)) {
    if(argc == 2) {
      PrintShips();
    }
    else if(argc == 3) {
      try {
	PrintShip(Ship::GetShip(converter::xws::GetShip(argv[2])));
      } catch(converter::xws::ShipNotFound snf) {
	std::vector<Ship> ss = Ship::FindShip(argv[2]);
	if(ss.size() == 1) { PrintShip(ss[0]); }
	else               { PrintShips(ss); }
      }
    }
    else {
      printOptions(settings);
    }
  }

  else if((strcmp(argv[1], "pilot") == 0)) {
    if(argc == 2) {
      PrintPilots();
    }
    else if(argc == 3) {
      try {
	Pilot p = Pilot::GetPilot(converter::xws::GetPilot(argv[2]));
	PrintPilot(p);
      }
      catch(converter::xws::PilotNotFound pnf) {
	std::vector<Pilot> ps = Pilot::FindPilot(argv[2]);
	if(ps.size() == 1) { PrintPilot(ps[0]); }
	else               { PrintPilots(ps); }
      }
    }
    else {
      printOptions(settings);
    }
  }

  else if((strcmp(argv[1], "upgrade") == 0)) {
    if(argc == 2) {
      PrintUpgrades();
    }
    else if(argc == 3) {
      try {
	Upgrade u = Upgrade::GetUpgrade(converter::xws::GetUpgrade(argv[2]));
	PrintUpgrade(u);
      }
      catch(converter::xws::UpgradeNotFound unf) {
	std::vector<Upgrade> us = Upgrade::FindUpgrade(argv[2]);
	if(us.size() == 1) { PrintUpgrade(us[0]); }
	else               { PrintUpgrades(us); }
      }
    }
    else {
      printOptions(settings);
    }
  }

  else if((strcmp(argv[1], "release") == 0)) {
    if     (argc == 2) { PrintRelease(); }
    else if(argc == 3) { PrintRelease(argv[2]); }
    else               { printOptions(settings); }
  }

  else if((strcmp(argv[1], "squad") == 0) && (argc==3)) {
    Squad sq = MakeSquad(argv[2]);
    PrintSquad(sq);
  }

  else if((strcmp(argv[1], "validate") == 0) && (argc==3)) {
    ValidateList(argv[2]);
  }

  else if((strcmp(argv[1], "img") == 0) && (argc==4)) {
    Squad sq = MakeSquad(argv[2]);
    GenerateImage(sq, argv[3]);
  }
  
  else if((strcmp(argv[1], "run") == 0) && (argc>=4)) {
    bool cannotPlay = false;
    std::string f1 = argv[2];
    std::string f2 = argv[3];

    // run
    printf("Running %s\n", GetVersionInfo(settings).c_str());
    printf("\n");

    // verify we have the fonts
    printf("Checking fonts...\n");
    {
      std::vector<std::pair<std::string,bool>> cf = CheckFonts();
      int fontlen=0;
      for(auto f : cf) { if(f.first.length() > fontlen) fontlen = f.first.length(); };
      for(auto f : cf) {
	printf("  %-*s - ", fontlen, f.first.c_str());
	if(f.second) {
	  printf("Ok\n");
	} else {
	  printf("%sNOT FOUND%s\n", CLR_ERR, CLR_DEF);
	  cannotPlay = true;
	}
      }
    }
    printf("\n");

    // verify the lists are present and valid
    printf("Checking lists...\n");
    {
      int listlen = f1.length();
      if(f2.length() > listlen) { listlen = f2.length(); }
      for(auto f : {f1, f2}) {
	printf("  %-*s - ", listlen, f.c_str());
	if(!fileExists(f)) {
	  printf("%sERROR\n    File not found%s\n", CLR_ERR, CLR_DEF);
	  cannotPlay = true;
	} else {
	  std::vector<std::string> issues = converter::xws::GetSquad(f).Validate();
	  if(issues.size() > 0) {
	    printf("%sISSUES%s:\n", CLR_WRN, CLR_DEF);
	    for(std::string i : issues) {
	      printf("    %s%s%s\n", CLR_WRN, i.c_str(), CLR_DEF);
	    }
	  } else {
	    printf("Ok\n");
	  }
	}
      }
    }
    printf("\n");
    // figure out what outdir to use
    printf("Checking outdir...\n");
    {
      // if outout file was passed in as argument, use it
      printf("Checking output file...\n");
      if(argc>=5) {
	printf("  using outdir from command line\n");
	settings.SetSetting("run.outdir", argv[4]);
      }

      // if the output file starts with '~', resolve it to home dir
      // user *can* resave this but doesn't have to
      //std::string outdir = settings.GetSetting("run.outdir");
      //if(outdir[0] == '~') {
      //  printf("  resolving ~\n");
      //  outdir.replace(0, 1, std::getenv("HOME"));
      //  settings.SetSetting("run.outdir", outdir);
      //}

      if(settings.GetSetting("run.outdir") == "") {
	printf("%sERROR\n    No outdir specified%s\n", CLR_ERR, CLR_DEF);
	cannotPlay = 1;
      }
    }
    printf("\n");

    if(cannotPlay) return 0;

    // run the game
    printf("Running game...\n");
    try{
      std::array<Squad, 2> players = { { converter::xws::GetSquad(f1), converter::xws::GetSquad(f2) } };
      Game g = Game(players, settings);
      g.Run();
    }
    catch(std::invalid_argument ia) {
      printf("Error loading list '%s'\n", ia.what());
      return 1;
    }

  }

  else {
    printf("Invalid Option\n");
    return 1;
  }

  return 0;
}
