#!/usr/bin/perl

mkdir "./test/";

sub RemoveColor {
    $ret = $_[0];
    $ret =~ s/\x1b\[[0-9;]*m//g; # remove the terminal color codes
    return $ret;
}

sub XwsConvert {
    $ret = $_[0];
    $ret =~ s/\s+//g;
    $ret = lc $ret;
    return $ret;
}

sub trim {
    $ret = $_[0];
    $ret =~ s/^\s+|\s+$//g;
    return $ret;
}

sub GetXwsShip {
    $target = $_[0];

    @ships = `../xhud ship`;
    foreach my $ship (@ships) {
	$ship = RemoveColor($ship);
	$ship =~ s/\[UNRELEASED]//;
	$ship = trim($ship);

	@tokens = split(/ +/, $ship);
	$shpxws = $tokens[@tokens-1];

	$ship =~ s/$shpxws//;
	$ship = trim($ship);

	if($ship eq $target) {
	    $xws = $shpxws;
	    last;
	}
    }
    return $xws;
}

@pilots = `../xhud pilot`;
print "Pilots";
foreach my $pilot (@pilots) {
    $pilot = trim($pilot);
    $pilot = RemoveColor($pilot);

    @factions = ("Rebel Alliance", "Galactic Empire", "Scum and Villainy", "Resistance", "First Order", "Galactic Republic", "Separatist Alliance");

    foreach my $faction (@factions) {
	$idx = index($pilot, $faction);
	if($idx != -1) {
	    $fac = XwsConvert($faction);
	    
	    $shipplt = substr($pilot, $idx + length($faction));
	    @tokens = split(/ +/, $shipplt);
	    $plt = $tokens[@tokens-1];

	    $ship = $shipplt;
	    $ship =~ s/$plt//;
	    $ship = trim($ship);
	    $shp = GetXwsShip($ship);
   	    last;
	}
    }

    if($fac eq "") {
	printf("[FAC]");
	continue;
    }
    if($plt eq "") {
	printf("[PLT]");
	continue;
    }
    if($shp eq "") {
	printf("[SHP]");
	continue;
    }

    my $xwsTemplate;
    my $filename = "./pilottest.xws.tmpl";

    open(my $fh, '<', $filename) or die "cannot open file $filename - $!";
    {
	local $/;
	$xwsTemplate = <$fh>;
    }
    close($fh);

    $xwsTemplate =~ s/{FACTION}/$fac/;
    $xwsTemplate =~ s/{PILOT}/$plt/;
    $xwsTemplate =~ s/{SHIP}/$shp/;

    $command = "cd .. && echo '$xwsTemplate' | ./xhud img -- ./testing/test/plt_$shp\_$fac\_$plt.png";

    system($command);
    print ".";
    #exit;
}
print "\n";

@upgrades = `../xhud upgrade`;
print "Upgrades";
@upgradetypes = ("Astromech", "Cannon", "Cargo", "Command", "Configuration", "Crew",
		 "Force Power", "Gunner", "Hardpoint", "Illicit", "Missile", "Modification",
		 "Sensor", "Tactical Relay", "Talent", "Team", "Tech", "Title", "Torpedo", "Turret");
foreach my $upgrade (@upgrades) {
    $upgrade = trim($upgrade);
    $upgrade = RemoveColor($upgrade);

    if($upgrade =~ /^U? +((?:\S|\s(?!\s))*) +((?:\S|\s(?!\s))*) +((?:\S|\s(?!\s))*) *$/) {
	$typ = XwsConvert($2);
	$upg = $3;
    } else {
	print "[PARSE_ERROR]";
    }

    if($typ eq "") {
	printf("[TYP]");
	continue;
    }
    if($upg eq "") {
	printf("[UPG]");
	continue;
    }

    my $xwsTemplate;
    my $filename = "./upgradetest.xws.tmpl";
    
    open(my $fh, '<', $filename) or die "cannot open file $filename - $!";
    {
	local $/;
	$xwsTemplate = <$fh>;
    }
    close($fh);

    $xwsTemplate =~ s/{TYPE}/$typ/;
    $xwsTemplate =~ s/{UPGRADE}/$upg/g;

    $command = "cd .. && echo '$xwsTemplate' | ./xhud img -- ./testing/test/upg_$typ\_$upg.png";

    $ret = system($command);
    if($ret) {
	print "($command)";
    }

    print ".";
}
print "\n";
